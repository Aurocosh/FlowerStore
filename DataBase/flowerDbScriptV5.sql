DROP VIEW IF EXISTS florist_tasks_summary;
DROP VIEW IF EXISTS florist_tasks;    
DROP VIEW IF EXISTS clients_orders;
DROP VIEW IF EXISTS mass_orders;    
DROP VIEW IF EXISTS manager_orders;
DROP VIEW IF EXISTS florist_tasks_results;    
DROP VIEW IF EXISTS clients_info;
DROP VIEW IF EXISTS supllies_summary;
DROP VIEW IF EXISTS supllies_added;
DROP VIEW IF EXISTS supllies_spent;

DROP TABLE IF EXISTS bouquet_tasks;
DROP TABLE IF EXISTS flowers_in_bouquet;
DROP TABLE IF EXISTS accessories_in_bouquet;
DROP TABLE IF EXISTS bouquets;
DROP TABLE IF EXISTS accessories_purchased;
DROP TABLE IF EXISTS supply_accessory_batch;
DROP TABLE IF EXISTS supply_flower_batch;
DROP TABLE IF EXISTS news;
DROP TABLE IF EXISTS purchases;
DROP TABLE IF EXISTS purchase_types;
DROP TABLE IF EXISTS basket_accessories;
DROP TABLE IF EXISTS basket_bouquets;
DROP TABLE IF EXISTS requests;
DROP TABLE IF EXISTS clients;
DROP TABLE IF EXISTS genders;
DROP TABLE IF EXISTS bouquet_statuses;
DROP TABLE IF EXISTS accessories_in_bouquet_template;
DROP TABLE IF EXISTS flowers_in_bouquet_template;
DROP TABLE IF EXISTS bouquet_templates;
DROP TABLE IF EXISTS flowers_in_new_bouquet_template;
DROP TABLE IF EXISTS accessories_in_new_bouquet_template;
DROP TABLE IF EXISTS supply_new_flower_batch;
DROP TABLE IF EXISTS supply_new_accessory_batch;
DROP TABLE IF EXISTS accessories;
DROP TABLE IF EXISTS flowers;
DROP TABLE IF EXISTS supplies;
DROP TABLE IF EXISTS supply_statuses;
DROP TABLE IF EXISTS employees;
DROP TABLE IF EXISTS user_groups;



CREATE TABLE user_groups (
    id serial primary key,
    title text    
);

INSERT INTO user_groups(id,title) VALUES (1,'Администратор');
INSERT INTO user_groups(id,title) VALUES (2,'Менеджер');
INSERT INTO user_groups(id,title) VALUES (3,'Флорист');

CREATE TABLE employees (
    id serial primary key,
    firstname text,
    lastname text,
    patronymic text,
	email text,
	password text,
	user_group_id integer REFERENCES user_groups(id),
	active boolean DEFAULT true
);

CREATE TABLE supply_statuses (
    id serial primary key,
    title text,
	is_received boolean DEFAULT false
);

INSERT INTO supply_statuses(id,title,is_received) VALUES(1,'Запланировано',false);
INSERT INTO supply_statuses(id,title,is_received) VALUES(2,'Отменено',false);
INSERT INTO supply_statuses(id,title,is_received) VALUES(3,'Получено',true);

CREATE TABLE supplies (
    id serial primary key,
    title text,
	created date NOT NULL DEFAULT current_date,
	planned date,
	received date,
	supply_status_id integer REFERENCES supply_statuses(id)
);

CREATE TABLE flowers (
    id serial primary key,
    title text,
	active boolean DEFAULT true
);

CREATE TABLE accessories (
    id serial primary key,
    title text,
    description text,
    price integer,
	can_be_sold boolean,
	active boolean DEFAULT true
);

CREATE TABLE supply_flower_batch (
    id serial primary key,
	supply_id integer REFERENCES supplies(id) ON DELETE CASCADE,
	flower_id integer REFERENCES flowers(id) ON DELETE CASCADE,
	count integer
);

CREATE TABLE supply_accessory_batch (
    id serial primary key,
	supply_id integer REFERENCES supplies(id) ON DELETE CASCADE,
	accessory_id integer REFERENCES accessories(id) ON DELETE CASCADE,
	count integer
);

CREATE TABLE bouquet_templates (
    id serial primary key,
    title text,
    description text,
	price integer DEFAULT 0,
    purchase_count integer DEFAULT 0,
	active boolean DEFAULT true
);

CREATE TABLE flowers_in_bouquet_template (
    id serial primary key,
	bouquet_template_id integer   REFERENCES bouquet_templates(id) ON DELETE CASCADE,
	flower_id integer   REFERENCES flowers(id) ON DELETE SET NULL,
	count integer DEFAULT 1
);

CREATE TABLE accessories_in_bouquet_template (
    id serial primary key,
	bouquet_template_id integer   REFERENCES bouquet_templates(id) ON DELETE CASCADE,
	accessory_id integer   REFERENCES accessories(id) ON DELETE SET NULL,
	count  integer DEFAULT 1
);

CREATE TABLE bouquet_statuses (
    id serial primary key,
    title text
);

INSERT INTO bouquet_statuses(id,title) VALUES (1,'В работе');
INSERT INTO bouquet_statuses(id,title) VALUES (2,'Выполнен');
INSERT INTO bouquet_statuses(id,title) VALUES (3,'Отправлен');
INSERT INTO bouquet_statuses(id,title) VALUES (4,'Отменен');

CREATE TABLE genders (
    id serial primary key,
    title text
);

INSERT INTO genders(id,title) VALUES (1,'Не указан');
INSERT INTO genders(id,title) VALUES (2,'Мужской');
INSERT INTO genders(id,title) VALUES (3,'Женский');


CREATE TABLE clients (
    id serial primary key,
    firstname text,
    lastname text,
    patronymic text,
	email text,
	password text,
    address text,
	phone text,
	gender_id integer REFERENCES genders(id)
);


CREATE TABLE requests (
    id serial primary key,
	client_id integer REFERENCES clients(id),
	employee_id integer REFERENCES employees(id) ON DELETE SET NULL,
    date date NOT NULL DEFAULT current_date
);

CREATE TABLE basket_bouquets (
    id serial primary key,
	client_id integer   REFERENCES clients(id) ON DELETE CASCADE,
	bouquet_template_id integer   REFERENCES bouquet_templates(id) ON DELETE CASCADE,
	count integer DEFAULT 1
);

CREATE TABLE basket_accessories (
    id serial primary key,
	client_id integer   REFERENCES clients(id) ON DELETE CASCADE,
	accessory_id integer   REFERENCES accessories(id) ON DELETE CASCADE,
	count integer DEFAULT 1
);

CREATE TABLE purchase_types (
    id serial primary key,
    title text,
	priority integer
);

INSERT INTO purchase_types(id,title,priority) VALUES (1,'Массовый заказ',0);
INSERT INTO purchase_types(id,title,priority) VALUES (2,'Частный клиент',1);
INSERT INTO purchase_types(id,title,priority) VALUES (3,'Внутренний заказ',2);

CREATE TABLE purchases (
    id serial primary key,
	client_id integer REFERENCES clients(id),
	employee_id integer REFERENCES employees(id) ON DELETE CASCADE,
	purchase_type_id integer REFERENCES purchase_types(id),
	date date,
	nulled date,	
	delivery date
);

CREATE TABLE news (
    id serial primary key,
	header text,
	description text,
	picture bytea,
	date date
);

CREATE TABLE accessories_purchased (
    id serial primary key,	
	accessory_id integer REFERENCES accessories(id) ON DELETE SET NULL,
	purchase_id integer REFERENCES purchases(id),
	price integer DEFAULT 0,
	count integer	 DEFAULT 1
);

CREATE TABLE bouquets (
    id serial primary key,
	bouquet_template_id integer REFERENCES bouquet_templates(id),
	purchase_id integer REFERENCES purchases(id),
	price integer DEFAULT 0,
	count integer	DEFAULT 1
);

CREATE TABLE bouquet_tasks (
    id serial primary key,	
	employee_id integer REFERENCES employees(id) ON DELETE CASCADE,
	bouquet_id integer REFERENCES bouquets(id),
	bouquet_status_id integer REFERENCES bouquet_statuses(id),
	created timestamp without time zone DEFAULT now(),
	completed timestamp without time zone
);

CREATE TABLE flowers_in_bouquet (
    id serial primary key,
	bouquet_id integer REFERENCES bouquets(id),
	flower_id integer REFERENCES flowers(id) ON DELETE SET NULL,
	count integer DEFAULT 1
);

CREATE TABLE accessories_in_bouquet (
    id serial primary key,
	bouquet_id integer REFERENCES bouquets(id),
	accessory_id integer REFERENCES accessories(id) ON DELETE SET NULL,
	count integer DEFAULT 1
);

CREATE TABLE flowers_in_new_bouquet_template (
    id serial primary key,
	employee_id integer REFERENCES employees(id) ON DELETE CASCADE,
	flower_id integer NOT NULL REFERENCES flowers(id) ON DELETE SET NULL,
	count integer DEFAULT 1
);

CREATE TABLE accessories_in_new_bouquet_template (
    id serial primary key,
	employee_id integer REFERENCES employees(id) ON DELETE CASCADE,
	accessory_id integer NOT NULL REFERENCES accessories(id) ON DELETE SET NULL,
	count integer DEFAULT 1
);

CREATE TABLE supply_new_flower_batch (
  id serial primary key,
	employee_id integer REFERENCES employees(id) ON DELETE CASCADE,
	flower_id integer REFERENCES flowers(id) ON DELETE CASCADE,
	count integer
);

CREATE TABLE supply_new_accessory_batch (
  id serial primary key,
	employee_id integer REFERENCES employees(id) ON DELETE CASCADE,
	accessory_id integer REFERENCES accessories(id) ON DELETE CASCADE,
	count integer
);

CREATE OR REPLACE FUNCTION make_a_purchase(current_client_id integer, delivery_date date)
    RETURNS boolean  AS
$$
DECLARE
  purchase_id integer;
  bouquet_count integer;
  accessory_count integer;
  got_all_requirements boolean;
  item_id integer;
	basket_items RECORD;	
BEGIN
    SELECT COUNT(*)
	INTO bouquet_count
	FROM basket_bouquets
	WHERE client_id = current_client_id;
		
  SELECT COUNT(*)
	INTO accessory_count
	FROM basket_accessories
	WHERE client_id = current_client_id;
		
	IF bouquet_count + accessory_count = 0 THEN
		RETURN false;
	END IF;
    
  WITH required_flowers AS (
    SELECT fbt.flower_id AS id,
      SUM(fbt.count) AS count
    FROM basket_bouquets AS bb
    JOIN flowers_in_bouquet_template AS fbt ON bb.bouquet_template_id = fbt.bouquet_template_id
    WHERE client_id = 1
    GROUP BY fbt.flower_id),
  required_accessories AS (
    SELECT abt.accessory_id AS id,
      SUM(abt.count) AS count
    FROM basket_bouquets AS bb
    JOIN accessories_in_bouquet_template AS abt ON bb.bouquet_template_id = abt.bouquet_template_id
    WHERE client_id = 1
    GROUP BY abt.accessory_id),
  available_supplyes AS (
    SELECT id,
      title,
      SUM(count) AS count,
      type
    FROM supllies_summary
    GROUP BY id,
      title,
      type),
  got_all_flowers AS (
    SELECT bool_and(COALESCE(avs.count,0) - rf.count >= 0)
    FROM required_flowers AS rf
    LEFT JOIN available_supplyes AS avs ON avs.id = rf.id
      AND avs.type = 0),
  got_all_accessories AS (
    SELECT bool_and(COALESCE(avs.count,0) - ra.count >= 0)
    FROM required_accessories AS ra
    LEFT JOIN available_supplyes AS avs ON avs.id = ra.id
      AND avs.type = 1)
  SELECT (SELECT * FROM got_all_flowers) AND (SELECT * FROM got_all_accessories)
  INTO got_all_requirements;
  
  IF NOT got_all_requirements THEN
		RETURN false;
	END IF;
	
  INSERT INTO purchases(client_id,purchase_type_id,delivery)
  VALUES(current_client_id,2,delivery_date)
  RETURNING id
  INTO purchase_id;

  FOR basket_items IN
    SELECT bouquet_template_id,count
    FROM basket_bouquets
    WHERE client_id = current_client_id
  LOOP
    INSERT INTO bouquets(bouquet_template_id,purchase_id,price,count)
    SELECT id,purchase_id,price,basket_items.count
    FROM bouquet_templates
    WHERE id = basket_items.bouquet_template_id
    RETURNING id
    INTO item_id;
    
    INSERT INTO flowers_in_bouquet(bouquet_id,flower_id,count)
    SELECT item_id,flower_id,count
    FROM flowers_in_bouquet_template
    WHERE bouquet_template_id = item_id;
        
    INSERT INTO accessories_in_bouquet(bouquet_id,accessory_id,count)
    SELECT item_id,accessory_id,count
    FROM accessories_in_bouquet_template
    WHERE bouquet_template_id = item_id;
  END LOOP;
  
  FOR basket_items IN
    SELECT accessory_id,count
    FROM basket_accessories
    WHERE client_id = current_client_id
  LOOP
    INSERT INTO accessories_purchased(accessory_id,purchase_id,price,count)
    SELECT id,purchase_id,price,basket_items.count
    FROM accessories
    WHERE id = basket_items.accessory_id;
  END LOOP;
  
  DELETE FROM basket_bouquets WHERE client_id = current_client_id;
  DELETE FROM basket_accessories WHERE client_id = current_client_id;
  
  RETURN true;
END;
$$
LANGUAGE 'plpgsql' VOLATILE;

CREATE OR REPLACE FUNCTION add_bouquet_to_basket(current_client_id integer, current_bouquet_id integer)
    RETURNS void  AS
$$
DECLARE
  existing_bouquet_id integer;
BEGIN
	SELECT id
	INTO existing_bouquet_id
	FROM basket_bouquets
	WHERE client_id = current_client_id
        AND bouquet_template_id = current_bouquet_id;
		
	IF existing_bouquet_id IS NOT NULL THEN
        UPDATE basket_bouquets
        SET count = count + 1
        WHERE id = existing_bouquet_id;
    ELSE
        INSERT INTO basket_bouquets(client_id,bouquet_template_id)
        VALUES(current_client_id,current_bouquet_id);
	END IF;	
END;
$$
LANGUAGE 'plpgsql' VOLATILE;

CREATE OR REPLACE FUNCTION make_a_purchase(current_client_id integer, delivery_date date)
    RETURNS boolean  AS
$$
DECLARE
  purchase_id integer;
  bouquet_count integer;
  accessory_count integer;
  got_all_requirements boolean;
  item_id integer;
	basket_items RECORD;	
BEGIN
    SELECT COUNT(*)
	INTO bouquet_count
	FROM basket_bouquets
	WHERE client_id = current_client_id;
		
  SELECT COUNT(*)
	INTO accessory_count
	FROM basket_accessories
	WHERE client_id = current_client_id;
		
	IF bouquet_count + accessory_count = 0 THEN
		RETURN false;
	END IF;
    
  WITH required_flowers AS (
    SELECT fbt.flower_id AS id,
      SUM(fbt.count) AS count
    FROM basket_bouquets AS bb
    JOIN flowers_in_bouquet_template AS fbt ON bb.bouquet_template_id = fbt.bouquet_template_id
    WHERE client_id = current_client_id
    GROUP BY fbt.flower_id),
  required_accessories AS (
    SELECT abt.accessory_id AS id,
      SUM(abt.count) AS count
    FROM basket_bouquets AS bb
    JOIN accessories_in_bouquet_template AS abt ON bb.bouquet_template_id = abt.bouquet_template_id
    WHERE client_id = current_client_id
    GROUP BY abt.accessory_id),
  available_supplyes AS (
    SELECT id,
      title,
      SUM(count) AS count,
      type
    FROM supllies_summary
    GROUP BY id,
      title,
      type),
  got_all_flowers AS (
    SELECT bool_and(COALESCE(avs.count,0) - rf.count >= 0)
    FROM required_flowers AS rf
    LEFT JOIN available_supplyes AS avs ON avs.id = rf.id
      AND avs.type = 0),
  got_all_accessories AS (
    SELECT bool_and(COALESCE(avs.count,0) - ra.count >= 0)
    FROM required_accessories AS ra
    LEFT JOIN available_supplyes AS avs ON avs.id = ra.id
      AND avs.type = 1)
  SELECT (SELECT * FROM got_all_flowers) AND (SELECT * FROM got_all_accessories)
  INTO got_all_requirements;
  
  IF NOT got_all_requirements THEN
		RETURN false;
	END IF;
	
  INSERT INTO purchases(client_id,purchase_type_id,delivery)
  VALUES(current_client_id,2,delivery_date)
  RETURNING id
  INTO purchase_id;

  FOR basket_items IN
    SELECT bouquet_template_id,count
    FROM basket_bouquets
    WHERE client_id = current_client_id
  LOOP
    INSERT INTO bouquets(bouquet_template_id,purchase_id,price,count)
    SELECT id,purchase_id,price,basket_items.count
    FROM bouquet_templates
    WHERE id = basket_items.bouquet_template_id
    RETURNING id
    INTO item_id;
    
    INSERT INTO flowers_in_bouquet(bouquet_id,flower_id,count)
    SELECT item_id,flower_id,count
    FROM flowers_in_bouquet_template
    WHERE bouquet_template_id = item_id;
        
    INSERT INTO accessories_in_bouquet(bouquet_id,accessory_id,count)
    SELECT item_id,accessory_id,count
    FROM accessories_in_bouquet_template
    WHERE bouquet_template_id = item_id;
  END LOOP;
  
  FOR basket_items IN
    SELECT accessory_id,count
    FROM basket_accessories
    WHERE client_id = current_client_id
  LOOP
    INSERT INTO accessories_purchased(accessory_id,purchase_id,price,count)
    SELECT id,purchase_id,price,basket_items.count
    FROM accessories
    WHERE id = basket_items.accessory_id;
  END LOOP;
  
  DELETE FROM basket_bouquets WHERE client_id = current_client_id;
  DELETE FROM basket_accessories WHERE client_id = current_client_id;
  
  RETURN true;
END;
$$
LANGUAGE 'plpgsql' VOLATILE;

CREATE OR REPLACE FUNCTION add_bouquet_to_basket(current_client_id integer, current_bouquet_id integer)
    RETURNS void  AS
$$
DECLARE
  existing_bouquet_id integer;
BEGIN
	SELECT id
	INTO existing_bouquet_id
	FROM basket_bouquets
	WHERE client_id = current_client_id
        AND bouquet_template_id = current_bouquet_id;
		
	IF existing_bouquet_id IS NOT NULL THEN
        UPDATE basket_bouquets
        SET count = count + 1
        WHERE id = existing_bouquet_id;
    ELSE
        INSERT INTO basket_bouquets(client_id,bouquet_template_id)
        VALUES(current_client_id,current_bouquet_id);
	END IF;	
END;
$$
LANGUAGE 'plpgsql' VOLATILE;

CREATE OR REPLACE FUNCTION add_accessory_to_basket(current_client_id integer, current_accessory_id integer)
    RETURNS void  AS
$$
DECLARE
  existing_accessory_id integer;
BEGIN
	SELECT id
	INTO existing_accessory_id
	FROM basket_accessories
	WHERE client_id = current_client_id
        AND accessory_id = current_accessory_id;
		
	IF existing_accessory_id IS NOT NULL THEN
        UPDATE basket_accessories
        SET count = count + 1
        WHERE id = existing_accessory_id;
    ELSE
        INSERT INTO basket_accessories(client_id,accessory_id)
        VALUES(current_client_id,current_accessory_id);
	END IF;	
END;
$$
LANGUAGE 'plpgsql' VOLATILE CALLED ON NULL INPUT;;

CREATE OR REPLACE FUNCTION add_accessory_to_basket(current_client_id integer, current_accessory_id integer)
    RETURNS void  AS
$$
DECLARE
  existing_accessory_id integer;
BEGIN
	SELECT id
	INTO existing_accessory_id
	FROM basket_accessories
	WHERE client_id = current_client_id
        AND accessory_id = current_accessory_id;
		
	IF existing_accessory_id IS NOT NULL THEN
        UPDATE basket_accessories
        SET count = count + 1
        WHERE id = existing_accessory_id;
    ELSE
        INSERT INTO basket_accessories(client_id,accessory_id)
        VALUES(current_client_id,current_accessory_id);
	END IF;	
END;
$$
LANGUAGE 'plpgsql' VOLATILE;

CREATE OR REPLACE FUNCTION assign_bouquet_task_to_employee(current_employee_id integer)
    RETURNS boolean  AS
$$
DECLARE
    task_count integer;
	new_bouquet_id integer;
	strresult text;
BEGIN
	SELECT COUNT(*)
	INTO task_count
	FROM bouquet_tasks
	WHERE employee_id = current_employee_id
		AND bouquet_status_id = 1;
		
	IF task_count > 0 THEN
		RETURN false;
	END IF;
	
	SELECT b.id
	INTO new_bouquet_id
	FROM bouquets AS b
	JOIN purchases AS p ON b.purchase_id = p.id
	JOIN purchase_types AS pt ON pt.id = p.purchase_type_id
	LEFT JOIN bouquet_tasks AS bt ON bt.bouquet_id = b.id
	WHERE bt.id IS NULL
		AND p.nulled IS NULL		
	ORDER BY pt.priority ASC
	LIMIT 1;
	
	INSERT INTO bouquet_tasks(employee_id,bouquet_id,bouquet_status_id)
	VALUES (current_employee_id,new_bouquet_id,1);
    
    RETURN true;
END;
$$
LANGUAGE 'plpgsql' VOLATILE;

CREATE OR REPLACE VIEW florist_tasks_summary AS
SELECT concat_ws(' ',e.lastname,e.firstname,e.patronymic) AS fio,
    pt.title AS purchase_type,
    count(*) AS task_count,
    SUM(bt.completed - bt.created) AS time_spent
FROM bouquet_tasks AS bt
JOIN bouquets AS b ON b.id = bt.bouquet_id
JOIN purchases AS p ON p.id = b.purchase_id
JOIN purchase_types AS pt ON pt.id = p.purchase_type_id
JOIN employees AS e ON e.id = bt.employee_id
WHERE bt.completed IS NOT NULL
GROUP BY e.id,
    pt.id
ORDER BY e.id,
    pt.id;
    
CREATE OR REPLACE VIEW florist_tasks AS
SELECT concat_ws(' ',e.lastname,e.firstname,e.patronymic) AS fio,
    bte.title AS bouquet_title,
    b.price AS bouquet_price,
    b.count AS bouquet_count,
    p.date AS purchase_date,
    p.delivery AS purchase_delivery,
    bt.created AS task_start,
    bt.completed AS task_completed,
    bs.title AS bouquet_status,
    pt.title AS purchase_type
FROM bouquet_tasks AS bt
JOIN bouquets AS b ON b.id = bt.bouquet_id
JOIN bouquet_templates AS bte ON bte.id = b.bouquet_template_id
JOIN purchases AS p ON p.id = b.purchase_id
JOIN purchase_types AS pt ON pt.id = p.purchase_type_id
JOIN employees AS e ON e.id = bt.employee_id
JOIN bouquet_statuses AS bs ON bs.id = bt.bouquet_status_id
ORDER BY p.date,
    bt.created,
    bt.completed,
    e.id,
    pt.id;
    
CREATE OR REPLACE VIEW clients_orders AS
SELECT concat_ws(' ',c.lastname,c.firstname,c.patronymic) AS fio,
    bte.title AS bouquet_title,
    b.price AS bouquet_price,
    b.count AS bouquet_count,
    p.date AS purchase_date,
    bt.created AS task_start,
    bt.completed AS task_completed,
    bs.title AS bouquet_status
FROM purchases AS p
JOIN clients AS c ON c.id = p.client_id
JOIN bouquets AS b ON b.purchase_id = p.id
JOIN bouquet_templates AS bte ON bte.id = b.bouquet_template_id
LEFT JOIN bouquet_tasks AS bt ON bt.bouquet_id = b.id
LEFT JOIN bouquet_statuses AS bs ON bs.id = bt.bouquet_status_id
WHERE p.purchase_type_id = 2
ORDER BY c.id,
    p.id;
    
CREATE OR REPLACE VIEW mass_orders AS
SELECT p.id AS purchase_id,
    concat_ws(' ',e.lastname,e.firstname,e.patronymic) AS employee_fio,
    concat_ws(' ',c.lastname,c.firstname,c.patronymic) AS client_fio,
    bte.title AS bouquet_title,
    b.price AS bouquet_price,
    b.count AS bouquet_count,
    p.date AS purchase_date,
    bt.created AS task_start,
    bt.completed AS task_completed,
    bs.title AS bouquet_status
FROM purchases AS p
JOIN employees AS e ON e.id = p.employee_id
JOIN clients AS c ON c.id = p.client_id
JOIN bouquets AS b ON b.purchase_id = p.id
JOIN bouquet_templates AS bte ON bte.id = b.bouquet_template_id
LEFT JOIN bouquet_tasks AS bt ON bt.bouquet_id = b.id
LEFT JOIN bouquet_statuses AS bs ON bs.id = bt.bouquet_status_id
WHERE p.purchase_type_id = 1
ORDER BY p.id,
    e.id,
    p.date;
    
CREATE OR REPLACE VIEW manager_orders AS
SELECT concat_ws(' ',e.lastname,e.firstname,e.patronymic) AS fio,
    bte.title AS bouquet_title,
    b.price AS bouquet_price,
    b.count AS bouquet_count,
    p.date AS purchase_date,
    bt.created AS task_start,
    bt.completed AS task_completed,
    bs.title AS bouquet_status
FROM purchases AS p
JOIN employees AS e ON e.id = p.employee_id
JOIN bouquets AS b ON b.purchase_id = p.id
JOIN bouquet_templates AS bte ON bte.id = b.bouquet_template_id
LEFT JOIN bouquet_tasks AS bt ON bt.bouquet_id = b.id
LEFT JOIN bouquet_statuses AS bs ON bs.id = bt.bouquet_status_id
WHERE p.purchase_type_id = 3
ORDER BY e.id,
    p.id,
    p.date;
    
CREATE OR REPLACE VIEW florist_tasks_results AS
SELECT pt.title AS purchase_type,
    COALESCE(bs.title,'Не назначено'),
    COUNT(*)
FROM purchases AS p
JOIN purchase_types AS pt ON pt.id = p.purchase_type_id
JOIN bouquets AS b ON b.purchase_id = p.id
LEFT JOIN bouquet_tasks AS bt ON bt.bouquet_id = b.id
LEFT JOIN bouquet_statuses AS bs ON bs.id = bt.bouquet_status_id
GROUP BY pt.id,
    bs.id
ORDER BY pt.id,
    bs.id;
    
CREATE OR REPLACE VIEW clients_info AS
SELECT concat_ws(' ',c.lastname,c.firstname,c.patronymic) AS fio,
    c.email,
    c.phone,
    g.title AS gender    
FROM clients AS c
LEFT JOIN genders AS g ON g.id = c.gender_id;

CREATE OR REPLACE VIEW supllies_spent AS
WITH bouquet_flower_count AS (
    SELECT fib.flower_id,
        fib.count,
        p.date
    FROM bouquets AS b
    JOIN purchases AS p ON p.id = b.purchase_id
    JOIN flowers_in_bouquet AS fib ON fib.bouquet_id = b.id
    LEFT JOIN bouquet_tasks AS bt ON bt.bouquet_id = b.id
    WHERE bt.bouquet_status_id != 4
        AND p.nulled IS NULL),
bouquet_accessory_count AS (
    SELECT aib.accessory_id,
        aib.count,
        p.date
    FROM bouquets AS b
    JOIN purchases AS p ON p.id = b.purchase_id
    JOIN accessories_in_bouquet AS aib ON aib.bouquet_id = b.id
    LEFT JOIN bouquet_tasks AS bt ON bt.bouquet_id = b.id
    WHERE bt.bouquet_status_id != 4
        AND p.nulled IS NULL)
SELECT f.id,
    f.title,
    bfc.count,
    bfc.date AS item_date,
    0 AS type
FROM flowers AS f
JOIN bouquet_flower_count AS bfc ON bfc.flower_id = f.id
UNION ALL
SELECT a.id,
    a.title,
    bac.count,
    bac.date AS item_date,
    1 AS type
FROM accessories AS a
JOIN bouquet_accessory_count AS bac ON bac.accessory_id = a.id;

CREATE OR REPLACE VIEW supllies_added AS
WITH supply_flower_count AS (
    SELECT sfb.flower_id,
        sfb.count,
        s.received
    FROM supply_flower_batch AS sfb
    JOIN supplies AS s ON s.id = sfb.supply_id
    WHERE s.supply_status_id != 2
        AND s.received IS NOT NULL),
supply_accessory_count AS (
    SELECT afb.accessory_id,
        afb.count,
        s.received
    FROM supply_accessory_batch AS afb
    JOIN supplies AS s ON s.id = afb.supply_id
    WHERE s.supply_status_id != 2
        AND s.received IS NOT NULL)
SELECT f.id,
    f.title,
    sfc.count,
    sfc.received AS item_date,
    0 AS type
FROM flowers AS f
JOIN supply_flower_count AS sfc ON sfc.flower_id = f.id
UNION ALL
SELECT a.id,
    a.title,
    sac.count,
    sac.received AS item_date,
    1 AS type
FROM accessories AS a
JOIN supply_accessory_count AS sac ON sac.accessory_id = a.id;

CREATE OR REPLACE VIEW supllies_summary AS
SELECT id,
    title,
    count,
    item_date,
    type
FROM supllies_added
UNION ALL
SELECT id,
    title,
    -count,
    item_date,
    type
FROM supllies_spent;

