Добавление букета в корзину
INSERT INTO basket_bouquets(client_id,bouquet_template_id) VALUES(?,?);

Обновление количества букетов в корзине
UPDATE basket_bouquets SET count = ? WHERE id = ?;

Удаление букета из корзины
DELETE FROM basket_bouquets WHERE id = ?;
DELETE FROM basket_accessories WHERE id = ?;


Добавление аксессуара в корзину
INSERT INTO basket_accessories(client_id,accessory_id) VALUES(?,?);

Обновление количества аксессуаров в корзине
UPDATE basket_accessories SET count = ? WHERE id = ?;

Удаление аксессуара из корзины
DELETE FROM basket_accessories WHERE id = ?;

Сумма стоимости товаров в корзине
SELECT SUM(cost)
FROM (
(SELECT SUM(bb.count * bt.price) AS cost
FROM basket_bouquets AS bb
JOIN bouquet_templates AS bt ON bt.id = bb.bouquet_template_id
WHERE bb.client_id = ?)
UNION ALL
(SELECT SUM(ba.count * a.price)
FROM basket_accessories AS ba
JOIN accessories AS a ON a.id = ba.accessory_id
WHERE ba.client_id = ?)) AS t

Получение id текущего задания флориста
SELECT id
FROM bouquet_tasks
WHERE bouquet_status_id = 2
LIMIT 1

Получение инфы о текущем задании флориста
SELECT bte.title,
	b.count
FROM bouquet_tasks AS bt
JOIN bouquets AS b ON b.id = bt.bouquet_id
JOIN bouquet_templates AS bte ON bte.id = b.bouquet_template_id
WHERE bt.id = ?

Список аксессуаров текущего задания флориста
(SELECT f.title,
	fib.count
FROM bouquet_tasks AS bt
JOIN bouquets AS b ON b.id = bt.bouquet_id
JOIN flowers_in_bouquet AS fib ON fib.bouquet_id = b.id
JOIN flowers AS f ON f.id = fib.flower_id
WHERE bt.id = ?)
UNION ALL
(SELECT f.title,
	fib.count
FROM bouquet_tasks AS bt
JOIN bouquets AS b ON b.id = bt.bouquet_id
JOIN accessories_in_bouquet AS aib ON aib.bouquet_id = b.id
JOIN accessories AS a ON a.id = aib.accessory_id
WHERE bt.id = ?)

Добавление нового сотрудника
INSERT INTO employees(lastname,firstname,patronymic,email,password,user_group_id)
VALUES (?,?,?,?,?,?);

Список сотрудников
SELECT e.id,
	e.firstname,
	e.lastname,
	e.patronymic,
	e.email,
	ug.title
FROM employees AS e
JOIN user_groups AS ug ON ug.id = e.user_group_id


Назначение сотруднику нового задания. Задание начнется автоматически. В параметры передается id сотрудника.
CREATE OR REPLACE FUNCTION assign_bouquet_task_to_employee(current_employee_id integer)
    RETURNS void  AS
$$
DECLARE
    task_count integer;
	new_bouquet_id integer;
	strresult text;
BEGIN
	SELECT COUNT(*)
	INTO task_count
	FROM bouquet_tasks
	WHERE employee_id = current_employee_id
		AND bouquet_status_id IN (1,2);
		
	IF task_count > 0 THEN
		RETURN;
	END IF;
	
	SELECT b.id
	INTO new_bouquet_id
	FROM bouquets AS b
	JOIN purchases AS p ON b.purchase_id = p.id
	JOIN purchase_types AS pt ON pt.id = p.purchase_type_id
	LEFT JOIN bouquet_tasks AS bt ON bt.bouquet_id = b.id
	WHERE bt.id IS NULL
		AND p.nulled IS NULL		
	ORDER BY pt.priority ASC
	LIMIT 1;
	
	INSERT INTO bouquet_tasks(employee_id,bouquet_id,bouquet_status_id,started)
	VALUES (current_employee_id,new_bouquet_id,2,current_date);	
END;
$$
LANGUAGE 'plpgsql' VOLATILE;

Создание нового клиента
INSERT INTO clients(lastname,firstname,patronymic,email,password,address,age,gender_id)
VALUES(?,?,?,?,?,?,?,?);

Получение информации о клиенте
SELECT lastname,
	firstname,
	patronymic,
	email,
	password,
	address,
	age,
	gender_id
FROM clients
WHERE id = ?

Обновление информации о клиенте
UPDATE clients
SET lastname = ?,
	firstname = ?,
	patronymic = ?,
	email = ?,
	address = ?,
	age = ?,
	gender_id = ?
WHERE id = ?
