﻿ <?php
	include "../database/database-open.php";

	// открываем сессию
	session_start();

	if(isset($_POST['completeTask']) 
		&& $_POST['completeTask'] == 'Завершить заказ') {
		$id = $_POST['bouquet_tasks_id'];
		$query = "UPDATE bouquet_tasks 
				SET bouquet_status_id = 2,
				completed = current_date
				WHERE id = '$id'";
		$result = pg_query($link, $query);
		header("Location: /florist.php");
		exit;
	} else if(isset($_POST['getNewTask']) 
		&& $_POST['getNewTask'] == 'Взять заказ') {
		$id = $_POST['employee_id'];
		$query = "select assign_bouquet_task_to_employee('$id')";
		if($result = pg_query($link, $query)) {
			header("Location: /florist.php");
			exit;
		} else {
			$GLOBALS['nothing'] = "noTasks";
			header("Location: /florist.php");
			exit;
		}
	}
 ?>