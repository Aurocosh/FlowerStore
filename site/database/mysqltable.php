﻿<?php
	if (!isset($is_table_editable)) { 
		$is_table_editable = false;
	}
	
	// Вывод информации из базы данных
	if ($result = pg_query($link,$query.' limit '.$perpage.' offset '.$perpage*$start_pos)) {
		echo "<div id=\"content-main\" class = \"content mid\" >";

		echo "<div class = \"pages-numbers content title top\">".$page_numbers."</div>";
		echo "<table cellspacing=\"0px\"> <tr> ";
				
		$columns = pg_num_fields($result); 
		$i = 0;
		while ($i < pg_num_fields($result))
		{			
			$field = pg_field_name($result, $i);			
			echo "<td class=\"caption_table\" width=\"auto\">$field</td>";
			$i = $i + 1;
		}
		
		if($is_table_editable){
			echo "<td class=\"caption_table\" width=\"auto\">Изменить</td>";
			echo "<td class=\"caption_table\" width=\"auto\">Удалить</td>";
		}
		echo "</tr>";
		//read field name
		while($row = pg_fetch_row($result)){
			echo "<tr>";
			for($i = 0; $i < $columns; $i++) { 
				if (isset($table_width[$i])) { 
					echo "<td class=\"content_table\" width=\"$table_width[$i]%\"> $row[$i] </td>";
				} else { 
					echo "<td class=\"content_table\" width=\"auto\"> $row[$i] </td>";
				}
			}
			if($is_table_editable){
				echo "<td class=\"content_table\" width=\"auto\"> 
					<Form Name =\"table_form\" Method =\"get\" ACTION = \"edit.php\">
					<INPUT TYPE = Submit VALUE = \"Изменить\">
					<INPUT type = \"hidden\" name = \"pageid\" value = \"$pageid\"> 
					<INPUT type = \"hidden\" name = \"queid\" value = \"$queid\"> 
					<INPUT type = \"hidden\" name = \"action\" value = \"edit\"> 
					<INPUT type = \"hidden\" name = \"id\" value = \"$row[0]\"> 
					</form>
				</td>";
				echo "<td class=\"content_table\" width=\"auto\"> 
					<Form Name =\"table_form\" Method =\"get\" ACTION = \"edit.php\">
					<INPUT TYPE = Submit VALUE = \"Удалить\">
					<INPUT type = \"hidden\" name = \"pageid\" value = \"$pageid\"> 
					<INPUT type = \"hidden\" name = \"queid\" value = \"$queid\"> 
					<INPUT type = \"hidden\" name = \"action\" value = \"delete-do\"> 
					<INPUT type = \"hidden\" name = \"id\" value = \"$row[0]\"> 
					</form>
				</td>";
			}
			echo "</tr>"
			;
		}
		
		echo "</tr> 
			</table>"."<div class = \"pages-numbers content title bot\">".$page_numbers."</div>"."</div>";
	}
?>