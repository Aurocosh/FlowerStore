<!DOCTYPE html>
<html lang="ru">

<head>
  <title>Flowery - авторизация</title>
  <meta name="Author" content="author">
  <meta name="Description" content="description">
  <meta name="Keywords" content="keywords">
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="../styles.css">
  <script>
    
  </script>
</head>
<style>
</style>

<body>

  <?php
	include "../menu.php";
	include "../database/database-open.php";

	// открываем сессию
	session_start();
	
	if($_POST['closeButton'] == 'Выйти')
	{
		session_unset();
		session_destroy();
		header("Location: ../registration.php");
		exit;
	} else if($_POST['logIn'] == 'Войти') {
		$email = $_POST['email'];
		$password = $_POST['password'];
		$query = "SELECT id, user_group_id FROM employees WHERE email ILIKE '$email' AND password = '$password'";
		if ($result = pg_query($link, $query)) {
			if($row = pg_fetch_row($result)) {
					$_SESSION['email'] = $email;
					$_SESSION['id'] = $row[0];
					$_SESSION['user_group_id'] = $row[1];
					header("Location: /shop/index.php");
					exit; 
			} else {
				$query = "SELECT id FROM clients WHERE email ILIKE '$email' AND password = '$password'";
				if ($result = pg_query($link, $query)) {
					if($row = pg_fetch_row($result)) {
						$_SESSION['email'] = $email;
						$_SESSION['id'] = $row[0];
						header("Location: /shop/index.php");
						exit; 
					} else {
						$info = "<h1> Ошибка: Проверьте введенные данные</h1>
						<div class=\"button\"><a href=\"/registration.php\">Назад</a></div>";
					}
				} 
			}
		} 
		
	} else if($_POST['register'] == 'Зарегистрироваться') {
		$regName = $_POST['regName'];
		$regLastname = $_POST['regLastname'];
		$regPatronymic = $_POST['regPatronymic'];
		$regEmail = $_POST['regEmail'];
		$regPassword = $_POST['regPassword'];
		$regAddress = $_POST['regAddress'];
		$regPhone = $_POST['regPhone'];
		$regGender = $_POST['regGender'];
		
		
		$query = "INSERT INTO clients(firstname,lastname,patronymic,email,password,address,phone,gender_id) VALUES('$regName','$regLastname','$regPatronymic','$regEmail','$regPassword','$regAddress','$regPhone','$regGender')";
		if ($result = pg_query($link, $query)) {
			$info = "<h1>Вы успешно зарегистрированы</h1>
			<div class=\"button\"><a href=\"../registration.php\">Назад на страницу входа и регистрации</a></div>";
		} else {
			$info = "<h1> Ошибка: Невозможно подключиться к серверу. Проверьте введенные данные</h1>
			<div class=\"button\"><a href=\"../registration.php\">Назад</a></div>";
		}
	}
  ?>

  <div class="main">
		<?php
			echo $info; 
		?>
  </div>
  <div style="clear: both;"></div>

    <?php
		include "../footer.php";
		include "../database/database-close.php";
	?>

</body>

</html>