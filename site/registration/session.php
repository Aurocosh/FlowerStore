<?php
	session_start();
  if(!isset($_SESSION['id']) || !isset($_SESSION['email'])){
		header("Location: /registration.php");
  }
  else{
    $user_id = $_SESSION['id'];
    $email = $_SESSION['email'];
    if(isset($_SESSION['user_group_id']))
      $user_group_id = $_SESSION['user_group_id'];
  }
?>