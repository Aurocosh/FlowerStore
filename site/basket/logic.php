<!DOCTYPE html>
<html lang="ru">

<head>
  <title>Flowery - корзина!</title>
  <meta name="Author" content="author">
  <meta name="Description" content="description">
  <meta name="Keywords" content="keywords">
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="/styles.css">
  <script src="/js/jquery.min.js"></script>
</head>
<body>
  <?php
  	include $_SERVER['DOCUMENT_ROOT']."/database/database-open.php";
    include $_SERVER['DOCUMENT_ROOT']."/registration/session.php";
    include "../menu.php";
  ?>
  <div class="main">
  <?php
    $delivery_date = isset($_POST['need_delivery']) ? "'".$_POST['delivery_date']."'" : 'NULL';
    $query = "SELECT make_a_purchase($user_id, $delivery_date)";
    $result = pg_query($link,$query);
    $row = pg_fetch_row($result);
    if($row[0] === "t")
    {
      echo "<p>Заказ был успешно совершен!</p>"; 
    }
    else
    {
      $query = 
      "SELECT *
      FROM (
        SELECT SUM(count) AS count
        FROM (
          SELECT COUNT(*) AS count
          FROM basket_bouquets
          WHERE client_id = $user_id
          UNION ALL
          SELECT COUNT(*) AS count
          FROM basket_accessories
          WHERE client_id = $user_id) AS t) AS tt
      WHERE count = 0";
    $result = pg_query($link,$query);
    if($row = pg_fetch_row($result))
      echo "<p>В корзине нет товаров!</p>";
    else {
      $query = 
        "WITH required_flowers AS (
          SELECT fbt.flower_id AS id,
            SUM(fbt.count) AS count
          FROM basket_bouquets AS bb
          JOIN flowers_in_bouquet_template AS fbt ON bb.bouquet_template_id = fbt.bouquet_template_id
          WHERE client_id = 1
          GROUP BY fbt.flower_id),
          required_accessories AS (
          SELECT abt.accessory_id AS id,
            SUM(abt.count) AS count
          FROM basket_bouquets AS bb
          JOIN accessories_in_bouquet_template AS abt ON bb.bouquet_template_id = abt.bouquet_template_id
          WHERE client_id = 1
          GROUP BY abt.accessory_id),
          available_supplyes AS (
          SELECT id,
            title,
            SUM(count) AS count,
            type
          FROM supllies_summary
          GROUP BY id,
            title,
            type),
          missing_flowers AS (
          SELECT f.title,
              -(COALESCE(avs.count,0) - rf.count)
          FROM required_flowers AS rf
          LEFT JOIN available_supplyes AS avs ON avs.id = rf.id
            AND avs.type = 0
          JOIN flowers AS f ON f.id = rf.id
          WHERE COALESCE(avs.count,0) - rf.count < 0),
          missing_accessories AS (
          SELECT a.title,
              -(COALESCE(avs.count,0) - ra.count)
          FROM required_accessories AS ra
          LEFT JOIN available_supplyes AS avs ON avs.id = ra.id
            AND avs.type = 1
          JOIN accessories AS a ON a.id = ra.id
          WHERE COALESCE(avs.count,0) - ra.count < 0)
          SELECT * FROM missing_flowers
          UNION ALL
          SELECT * FROM missing_accessories";
        $result = pg_query($link,$query);
        $rows = pg_num_rows($result);
        if($rows > 0)
        {
          echo "<p>Покупка не возможна! Не хватает материалов</p>"; 
          while($row = pg_fetch_row($result)){ ?>
          <div class="busket-list">
            <div class="busket-item"><?=$row[0]?></div>
            <div class="busket-item"><?=$row[1]?></div>
          </div>
          <?
          }
        }
      }
    }
  ?>
      
  </div>
  <div style="clear: both;"></div>

  <?php
	include $_SERVER['DOCUMENT_ROOT']."/database/database-close.php";
  include "../footer.php";
  ?>
</body>

</html>