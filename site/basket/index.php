<!DOCTYPE html>
<html lang="ru">

<head>
  <title>Flowery - корзина!</title>
  <meta name="Author" content="author">
  <meta name="Description" content="description">
  <meta name="Keywords" content="keywords">
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="/styles.css">
  <script src="/js/jquery.min.js"></script>
  <script src="/js/index.js"></script>
  <script>
    function deleteItemFromBusket(id) {
      $.ajax({
        url: "/basket/delete.php",
        data: "idItem=" + id,
        cache: false,
        success: function(html) {
          $.ajax({
        url: "/basket/show.php",
        cache: false,
        success: function(html) {
          $("#busketContent").html(html);
          counter();
        }
      });
        }
      });
    }
    
    $(document).ready(function() {
      $.ajax({
        url: "/basket/show.php",
        cache: false,
        success: function(html) {
          $("#busketContent").html(html);
          counter();
        }
      });
    });
  </script>
</head>
<style>
</style>

<body>
  <?php
  include "../menu.php";
  include "../registration/session.php";
  include "../database/database-open.php";
  
  if(isset($user_group_id))
    header("Location: /shop");
  ?>
  <div class="main">

    <div id="busketContent">

    </div>

    <div style="text-align: right; font-size: 20px; padding-top: 10px; width: 100%;">
      <p id="final_price">Итого: 0</p>
    </div>
    <div style="font-size: 18px;">
      <!-- <h2>Уточнения о доставке</h2> -->
      <form id="form-order" method="post" action="/basket/logic.php">
        <input type="checkbox" name="need_delivery" onClick="this.checked ? document.getElementById('fut-delivery').style.display='block' : document.getElementById('fut-delivery').style.display='none'"> Отложить доставку
        <div style="display:none;" id="fut-delivery">
          <input type="date" name="delivery_date" value="2018-01-01"/>
        </div> 
        <input type="submit" class="busket-button" value="Заказать"></input>
      </form> 
    </div>

  </div>
  <div style="clear: both;"></div>
  <script>
  </script>
  <?php
  include "../footer.php";
  include "../database/database-close.php";
  ?>
</body>

</html>