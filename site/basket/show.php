<?
include $_SERVER['DOCUMENT_ROOT']."/database/database-open.php";
include $_SERVER['DOCUMENT_ROOT']."/registration/session.php";

$query = 
"(SELECT bb.id,0,bt.title,bt.price,bb.count
				FROM clients AS c
				JOIN basket_bouquets AS bb ON bb.client_id = c.id
				JOIN bouquet_templates AS bt ON bt.id = bb.bouquet_template_id
        WHERE c.id = $user_id
				ORDER BY id)
				UNION ALL
				(SELECT ba.id,1,a.title,a.price,ba.count
				FROM clients AS c
				JOIN basket_accessories AS ba ON ba.client_id = c.id
				JOIN accessories AS a ON a.id = ba.accessory_id
        WHERE c.id = $user_id
				ORDER BY id)";

		if ($result = pg_query($link,$query)) {
			while($row = pg_fetch_row($result)){ ?>
			<div class="busket-list" id="<?=$row[0]?>" itemType="<?=$row[1]?>">
				<div class="busket-item" style="width: 5%;"><img style="width: 50px" src="/images/flower.png" /></div>
				<div class="busket-item" style="width: 40%;">
					<a href="/shop.php/bouquet.php"><?=$row[2]?></a>
				</div>
				<div class="busket-item" style="width: 10%">
					<div class="amount">
						<span class="down">-</span>
						<input type="text" class="busket_count" title="<?=$row[0].'_'.$row[1]?>" value="<?=$row[4]?>" />
						<span class="up">+</span>
					</div>
				</div>
				<div class="busket-item price_busket" style="width: 10%"><span class="price_bus"><?=$row[3]?></span> &#8381;</div>
				<div class="busket-item" style="width: 10%"><a id="<?=$row[0].'_'.$row[1]?>" href="delete.html?id=2" target="_blank" onclick="return confirmDelete(this);" class="delete-link need-to-confirm" >
          <span class="maintext">&#10008; Удалить</span>
          <span class="confirmation">&#10004; Уверены?</span>
				</a></div>
			</div>
	<?	}
	}
	?>
<script type="text/javascript">
	$(document).ready(function () {
      $('.down').click(function () {
        var $input = $(this).parent().find('input');
        var count = parseInt($input.val()) - 1;
        count = count < 1 ? 1 : count;
        $input.val(count);
        $input.change();
        var temp, attr, id;
        temp = $input.attr('title').indexOf('_');
        id = parseInt($input.attr('title').substring(0,temp));
        attr = parseInt($input.attr('title').substring(temp+1));
  $.ajax({
        url: "/basket/increment.php",
        data: {id: id, attr: attr, count: $input.val()},
        cache: false,
      });

        return false;
      });
      $('.up').click(function () {
        var $input = $(this).parent().find('input');
        $input.val(parseInt($input.val()) + 1);
        var temp, attr, id;
        temp = $input.attr('title').indexOf('_');
        id = parseInt($input.attr('title').substring(0,temp));
        attr = parseInt($input.attr('title').substring(temp+1));
        $input.change();

  $.ajax({
        url: "/basket/increment.php",
        data: {id: id, attr: attr, count: $input.val()},
        cache: false,
      });

        return false;
      });

      $('.busket_count').change(function(){
      counter();
    })

          });

function counter() {
      var prices = document.getElementsByClassName("price_bus");
      var counts = document.getElementsByClassName("busket_count");
      var final = 0;
      for (var i = 0; i < prices.length; i++) {
        final += prices[i].innerText * counts[i].value;
      }
      document.getElementById('final_price').innerText = "Итого: " + final
    }

    
</script>
	<?

	include $_SERVER['DOCUMENT_ROOT']."/database/database-close.php";
?>