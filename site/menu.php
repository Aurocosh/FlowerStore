<div style="position: fixed;  z-index: 4;background: #764E80; width: 100%;">
<div class="menu-list">
  <div class="item"><a href="/"><img height="30px" src="/images/home.svg"></a></div>
  <div class="item"><a href="/basket"><img height="30px" src="/images/busket.svg"></a></div>
  <div class="item" style="width: 100%"><img class="navbar-logo" height="80px" src="/images/logo.png"></div>
  <div class="item"><a href="/shop/index.php">Магазин</a></div>
  <div class="item" style="width: 140px;"><a href="/decoration/index.php">Оформление праздников</a></div>
  <div class="item" style="width: 140px;"><a href="/about.php">О нас</a></div>
  
  <?php
  if (session_status() == PHP_SESSION_NONE) {
    session_start();
  }  
	if(isset($_SESSION['email'])) {		
		if(isset($_SESSION['user_group_id'])) {
			// 1 - admin, 2 - manager, 3 - florist
			switch($_SESSION['user_group_id']) {
				case 1:
					$lk = "/admin/index.php";
					break;
				case 2:
					$lk = "/manager/index.php";
					break;
				case 3:
					$lk = "/florist.php";
					break;
			}
		} else {
			// client
			$lk = "/client";
		}
		echo "<div class=\"item\" style=\"width: 140px;\"><a href=\"$lk\">Личный кабинет</a></div>"; 
	} else {
		echo "<div class=\"item\" style=\"width: 140px;\"><a href=\"/registration.php\">Вход/ Регистрация</a></div>"; 
	}
  ?>
  
</div>
</div>