<!DOCTYPE html>
<html lang="ru">

<head>
  <title>Flowery - личный кабинет</title>
  <meta name="Author" content="author">
  <meta name="Description" content="description">
  <meta name="Keywords" content="keywords">
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="/styles.css">
  <script src="/js/jquery.min.js"></script>
  <script src="/js/index.js"></script>
  <script>  
    function saveChanges() {
      var e = document.getElementById("some_id");
      var gender_id = e.options[e.selectedIndex].value;
      var info ={
        firstname: document.getElementById('name').value,
          lastname: document.getElementById('lastname').value,
          patronomyc: document.getElementById('patronomyc').value,
          gender: gender_id,
          phone: document.getElementById('phone').value,
          address: document.getElementById('address').value,
          email: document.getElementById('email').value,
          password: document.getElementById('password').value
      };
      console.log(info)
      $.ajax({
        url: "/client/logic.php",
        data: info,
        cache: false,
        success: function(data) {
          alert(data);
        }
      });
    }
      </script>
    </head>
    <style>
    .table {
      display: table;
    }

    .row {
      display: table-row;
      height: 30px;
    }

    .cell {
      display: table-cell;
      padding-top: 10px;
    }

    .cell:first-child{
      width: 100px;
      padding-left: 10px;
    }

    .cell:nth-child(2){
      width: 200px;
    }

    .row:nth-child(odd){
      background-color: rgba(182, 204, 177, 0.3);
    }

    .row:nth-child(even){
      background-color: rgba(182, 204, 177, 0.5);
    }
  </style>

  <body>
    <?php
    include "../menu.php";
    include "../database/database-open.php";
    include "../registration/session.php";
    ?>
    <div class="main">
      <?
      
      
      
      ?> 
      <div style="width: 69%; float: left;">
        <h2>История покупок</h2>
        <?
          $query=
          "(SELECT bte.title AS bouquet_title,
              b.price AS bouquet_price,
              b.count AS bouquet_count,
              p.date AS purchase_date,
              bt.created AS task_start,
              bt.completed AS task_completed,
              bs.title AS bouquet_status
          FROM purchases AS p
          JOIN clients AS c ON c.id = p.client_id
          JOIN bouquets AS b ON b.purchase_id = p.id
          JOIN bouquet_templates AS bte ON bte.id = b.bouquet_template_id
          LEFT JOIN bouquet_tasks AS bt ON bt.bouquet_id = b.id
          LEFT JOIN bouquet_statuses AS bs ON bs.id = bt.bouquet_status_id
          WHERE c.id = $user_id
          ORDER BY p.id)
          UNION ALL
          (SELECT a.title AS bouquet_title,
              ap.price AS bouquet_price,
              ap.count AS bouquet_count,
              p.date AS purchase_date,
              NULL AS task_start,
              NULL AS task_completed,
              NULL AS bouquet_status
          FROM purchases AS p
          JOIN clients AS c ON c.id = p.client_id
          JOIN accessories_purchased AS ap ON ap.purchase_id = p.id
          JOIN accessories AS a ON a.id = ap.accessory_id
          WHERE c.id = $user_id
          ORDER BY p.id)";
          $result = pg_query($link, $query);
          while($row= pg_fetch_row($result)) {
        ?>
      
        <div class="busket-list" id="<?=$row[0]?>" itemType="<?=$row[1]?>">
          <div class="busket-item" style="width: 10%;"><?=$row[0]?></div>
          <div class="busket-item" style="width: 20%;"><?=$row[1]?></div>
          <div class="busket-item" style="width: 10%;"><?=$row[2]?></div>
          <div class="busket-item" style="width: 10%;"><?=$row[3]?></div>
          <div class="busket-item" style="width: 10%;"><?=$row[4]?></div>
        </div>
      <?
          }
        $query= "SELECT firstname, lastname, patronymic, gender_id, phone, address, email, password  FROM clients WHERE id=$user_id";
        $result = pg_query($link, $query);
        $row= pg_fetch_row($result);
      ?> 
      </div>
      <div style="width: 350px; float: right;">
        <div class="table">
          <div class="row">
            <div class="cell">Имя</div>
            <div class="cell"><input type="text" id="name" value="<?=$row[0]?>"></div>
          </div>
          <div class="row">
            <div class="cell">Фамилия</div>
            <div class="cell"><input type="text" id="lastname" value="<?=$row[1]?>"></div>
          </div>
          <div class="row">
            <div class="cell">Отчество</div>
            <div class="cell"><input type="text" id="patronomyc" value="<?=$row[2]?>"></div>
          </div>
          <div class="row">
            <div class="cell">Пол</div>
            <div class="cell">
              <select id="some_id" style="width: 90%; padding-bottom: 10px;">
                <option value="1" <?if ($row[3]=="1") echo 'selected'?>>Не указан</option>
                <option value="2" <?if ($row[3]=="2") echo 'selected'?>>Мужской</option>
                <option value="3" <?if ($row[3]=='3') echo 'selected'?>>Женский</option>
              </select>
            </div>
          </div>
          <div class="row">
            <div class="cell">Телефон</div>
            <div class="cell"><input type="tel" id="phone" value="<?=$row[4]?>"></div>
          </div>
          <div class="row">
            <div class="cell">Адрес</div>
            <div class="cell"><input type="text" id="address" value="<?=$row[5]?>"></div>
          </div>
          <div class="row">
            <div class="cell">E-mail</div>
            <div class="cell"><input type="email" id="email" value="<?=$row[6]?>"></div>
          </div>
          <div class="row">
            <div class="cell">Пароль</div>
            <div class="cell"><input type="password" id="password" value="<?=$row[7]?>"></div>
          </div>
        </div>
        <br>
        <button onclick="saveChanges()" style="width: 155px;">Сохранить</button>
        <form method="post" action="/registration/authorization.php">  
          <input id="closeButton" type="submit" name="closeButton" class="filter_button" style="width: 155px;" value="Выйти"></input>
        </form>
      </div>
    </div>
    <div style="clear: both;"></div>
    <?php
    include "../footer.php";
    include "../database/database-close.php";
    ?>
  </body>

  </html>