<!DOCTYPE html>
<html lang="ru">

<head>
  <title>Flowery - работа для флориста</title>
  <meta name="Author" content="author">
  <meta name="Description" content="description">
  <meta name="Keywords" content="keywords">
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="styles.css">
  <script src="js/jquery.min.js"></script>
  <script src="js/index.js"></script>
  <script>
    
  </script>
</head>
<style>
.button-req {
	font-size: 16pt;
	padding-bottom: 5px;
}
h2 {
	margin-bottom: 10px;
}
</style>

<body>

  <?php
	include "menu.php";
	include "./database/database-open.php";
	
	session_start();
  ?>

  <div class="main" align="center">
    <div>
	
		<?php
			
			
			$id = $_SESSION['id'];
			
			$query = "SELECT id
					FROM bouquet_tasks
					WHERE bouquet_status_id = 1
					AND employee_id = '$id'
					LIMIT 1";
					
			echo "<form id=\"form\" name=\"form\" action=\"./database/getOrCompleteFloristTask.php\" method=\"post\">";
					
			$result = pg_query($link, $query);
			if($row = pg_fetch_row($result)) {
				$bouquet_tasks_id = $row[0];
				$query = "SELECT bte.title,
							b.count
						FROM bouquet_tasks AS bt
						JOIN bouquets AS b ON b.id = bt.bouquet_id
						JOIN bouquet_templates AS bte ON bte.id = b.bouquet_template_id
						WHERE bt.id = '$bouquet_tasks_id'";
						
				if ($result = pg_query($link, $query)) {
					if($row = pg_fetch_row($result)) {
						echo "<div id=\"florist_info\">";
						
						echo "<div style=\"overflow: hidden;\" class=\"busket-list\">
							  <div class=\"busket-item\"><img style=\"width: 100px\" src=\"/images/flower.png\" /></div>
							  <div class=\"busket-item\" style=\"width: 100%; text-align: center;\"><a href=\"#win1\">$row[0]</a></div>
							  <div class=\"busket-item\">Количество: $row[1]</div>
							</div>";
							
						echo "<h2 style=\"margin-top: 30px; margin-bottom: 10px\">Состав букета:</h2>";
							
						$query = "(SELECT f.title,
										fib.count
									FROM bouquet_tasks AS bt
									JOIN bouquets AS b ON b.id = bt.bouquet_id
									JOIN flowers_in_bouquet AS fib ON fib.bouquet_id = b.id
									JOIN flowers AS f ON f.id = fib.flower_id
									WHERE bt.id = '$bouquet_tasks_id')
									UNION ALL
									(SELECT a.title,
										aib.count
									FROM bouquet_tasks AS bt
									JOIN bouquets AS b ON b.id = bt.bouquet_id
									JOIN accessories_in_bouquet AS aib ON aib.bouquet_id = b.id
									JOIN accessories AS a ON a.id = aib.accessory_id
									WHERE bt.id = '$bouquet_tasks_id')";
									
						if($result = pg_query($link, $query)) {
							while($row = pg_fetch_row($result)) {
								echo "<div style=\"overflow: hidden;\" class=\"busket-list\">
										<div class=\"busket-item\" style=\"width: 100%; text-align: center;\">$row[0]</div>
										<div class=\"busket-item\">Количество: $row[1]</div>
									  </div>";
							}
							
							
							echo "<input type=\"submit\" class=\"button-req\" name = \"completeTask\" style=\" margin-top: 20px;\" value=\"Завершить заказ\"/>
								  <input type=\"text\" name=\"bouquet_tasks_id\" value=\"$bouquet_tasks_id\" hidden/>";
						}
						
						echo "</div>";
					}
				}
			} else {
				if(isset($GLOBALS['nothing'])
				&& $GLOBALS['nothing'] == "noTasks") {
					$GLOBALS['nothing'] = "";
					echo "<h2>Свободных задач нет</h2>";
				} else {
					echo "<h2>На данный момент никаких заказов не назначено</h2>";
				}
				echo "
						<input type=\"submit\" class=\"button-req\" name = \"getNewTask\" value = \"Взять заказ\"/>
						<input type=\"text\" name=\"employee_id\" value=\"$id\" hidden/>";
			}
			echo "</form>";

			if(isset($_SESSION['email']) && isset($_SESSION['id'])){
			?>
			<form method="post" action="./registration/authorization.php">	
				  <input id="closeButton" class="button-req" type="submit" name="closeButton" value="Выйти"></input>
						</form>
						<?
			}
		?>
    </div>
  </div>
  <div style="clear: both;"></div>

    <?php
		include "footer.php";
		include "./database/database-close.php";
	?>

</body>

</html>