<!DOCTYPE html>
<html lang="ru">

<head>
  <title>Flowery - вход/регистрация</title>
  <meta name="Author" content="author">
  <meta name="Description" content="description">
  <meta name="Keywords" content="keywords">
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="styles.css">
  <!-- <link rel="stylesheet" type="text/css" href="styleLogIn.css"> -->
  <script src="js/jquery.min.js"></script>
  <script src="js/index.js"></script>
  <script>
    
  </script>
</head>
<style>
.button-req {
  width: 200px;
  float: none;
}
h2{
  margin-bottom: 10px;
}
</style>

<body>

  <?php
	include "menu.php";
	include "./database/database-open.php";
	
	session_start();
	if(isset($_SESSION['email'])){
		header("Location: index.php");
		exit;
	}
  ?>

  <div class="main" align="center">
<div style="width: 60%" >
  <div class="menu-popup">
    <h2>Вход</h2>
    <form id="form" name="form" action="./registration/authorization.php" method="post">
		<div id="block">
			<input type="text" name="email" id="email" placeholder="E-mail" required/> <br>
			<input type="password" name="password" id="password" placeholder="Пароль" required /> <br>
			<input class="button-req" type="submit" id="submit" name="logIn" value="Войти"/>
		</div>
	</form>
  </div>
  <div class="menu-popup">
    <h2>Регистрация</h2>
    <form id="formReg" name="form" action="./registration/authorization.php" method="post">
      <input type="text" placeholder="Имя" name="regName" required /> <br>
      <input type="text" placeholder="Фамилия" name="regLastname" required /> <br>
      <input type="text" placeholder="Отчество" name="regPatronymic" required /> <br>
      <input type="text" placeholder="E-mail" name="regEmail" required /> <br>
      <input type="password" placeholder="Пароль" name="regPassword" required /> <br>
	  <input type="text" placeholder="Адрес проживания" name="regAddress" required /> <br>
	  <input type="tel" placeholder="Телефон" name="regPhone" required /> <br>

     Пол <select name="regGender" style="margin-bottom: 10px; width: 140px; font-size: 18pt;">
          <option value="1">Не указан</option>
          <option value="2">Мужской</option>
          <option value="3">Женский</option>
      </select> <br>

      <input type="submit" name="register" class="button-req" value="Зарегистрироваться" /> <br>
    </form>
  </div>
</div>
  </div>
  <div style="clear: both;"></div>

    <?php
		include "footer.php";
		include "./database/database-close.php";
	?>

</body>

</html>