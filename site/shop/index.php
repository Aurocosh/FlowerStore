<!DOCTYPE html>
<html lang="ru">

<head>
	<title>Flowery - магазин!</title>
	<meta name="Author" content="author">
	<meta name="Description" content="description">
	<meta name="Keywords" content="keywords">
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="/styles.css">
<!--  <script src="http://code.jquery.com/jquery-latest.js"></script>-->

</head>
  <style>
 	.filter_button{
 		margin-top: 10px;
 		margin-left: 10px;
 	}
  </style>
<body>
  <!-- MENU -->
  <?php
    include "../menu.php";
    include "../registration/session.php";
    include "../database/database-open.php";
  ?>

  <div class="main">
  
  <div id="left-col">
    <form method="post" action="index.php">
		  <div class="filter">
			<h3>Фильтр</h3> <br>
			<div class="filter_attr">Цветы</div> <br>
			  <div class="filter_list">
					Название:<br>
					<input name="searchStr" class = "side-menu-field" type=TEXT value="<?php echo isset($_POST['searchStr']) ? $_POST['searchStr'] : '' ?>" > <br>

					<?php
					$flowers = isset($_POST['flowers']) ? $_POST['flowers'] : null;		
					$query = 'SELECT id,title
						FROM flowers
						WHERE active';
					
					if ($result = pg_query($link,$query)) {
						while($row = pg_fetch_row($result)){
							$isChecked = $flowers !== null ? in_array($row[0],$flowers) : false;							
							$state = $isChecked ? 'checked' : '';
							echo "<input type=\"checkbox\" name=\"flowers[]\" value=\"$row[0]\" $state > $row[1]<br>";
						}
					}
					?>
			  </div>
			</div>
      <input type=submit class="filter_button" value="Применить"/>
    </form>
    <form method="post" action="index.php">
    	<input type=submit class="filter_button" value="Сбросить"/>
    </form>
  </div>	

    <div id="right-col">
      <div class="center-shop">
		<?php
			$titleFilter = '';
			$flowerFilter = '';
		
			if (isset($_POST['searchStr']) && $_POST['searchStr'] !== '') {
				$searchStr =  $_POST['searchStr'];
				$titleFilter = "AND title ILIKE '%$searchStr%'";
			}
			
			if (isset($_POST['flowers']) && !empty($_POST['flowers'])) {
				$searchStr =  implode(",", $_POST['flowers']); ;
				$flowerFilter = "HAVING array_agg(fbt.flower_id) @> ARRAY[$searchStr]";
			}
			
			$query = 
				"SELECT bt.id,bt.price,bt.title
				FROM bouquet_templates AS bt
				JOIN flowers_in_bouquet_template AS fbt ON fbt.bouquet_template_id = bt.id
				WHERE bt.active
					$titleFilter
        GROUP BY bt.id,bt.price,bt.title
					$flowerFilter
				ORDER BY bt.id,bt.title";
			$result = pg_query($link,$query);
      while($row = pg_fetch_row($result)){
      ?>
        <div class="flower_shop" id="$row[0]">
        <form id="bouquet_submit" method="post" action="/shop/logic/addBouquetToBasket.php">
          <input name = "bouquet_id" type="hidden" value="<?php echo $row[0]?>"/>
          <div id="item" style="background: url('/images/flower.png'); background-size: cover;" onClick="<?= !isset($user_group_id)? "this.parentNode.submit();return false;" : "" ?>">
            <p id="busket-holder"><img style="height: 40px;" src="/images/busket.svg" /></p>
            <div class="price"><?php echo $row[1] ?> &#8381;</div>
          </div>
        </form>
        <form id="bouquet_submit" method="post" action="/shop/bouquet.php">
          <input name = "bouquet_id" type="hidden" value="<?php echo $row[0]?>"/>
          <a href="" onClick=" this.parentNode.submit(); return false;" >Букет <?php echo $row[2]?></a>
        </form>
        </div>
			<?php
      } 
      
      $query = 
				"SELECT id,price,title
				FROM accessories
        WHERE active
					$titleFilter
				ORDER BY title";
			$result = pg_query($link,$query);
      while($row = pg_fetch_row($result)){
      ?>
        <div class="flower_shop" id="$row[0]">
          <form id="bouquet_submit" method="post" action="/shop/logic/addAccessoryToBasket.php">
            <input name = "accessory_id" type="hidden" value="<?php echo $row[0]?>"/>
            <div id="item" style="background: url('/images/flower.png'); background-size: cover;" onClick=" this.parentNode.submit(); return false;">
              <p id="busket-holder"><img style="height: 40px;" src="/images/busket.svg" /></p>
              <div class="price"><?php echo $row[1] ?> &#8381;</div>
            </div>
          </form>
          <form id="bouquet_submit" method="post" action="/shop/accessory.php">
            <input name = "accessory_id" type="hidden" value="<?php echo $row[0]?>"/>
            <a href="" onClick=" this.parentNode.submit(); return false;" >Аксессуар <?php echo $row[2]?></a>
          </form>
        </div>
      <?php
      }
      ?>
	  
      </div>
    </div>

  </div>
  <div style="clear: both;"></div>
	<?php
		include "../footer.php";
		include "../database/database-close.php";
	?>
</body>

</html>