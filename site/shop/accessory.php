<!DOCTYPE html>
<html lang="ru">

<head>
  <title>Flowery - об аксессуаре!</title>
  <meta name="Author" content="author">
  <meta name="Description" content="description">
  <meta name="Keywords" content="keywords">
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="/styles.css">
  <script src="http://code.jquery.com/jquery-latest.js"></script>
</head>
<style type="text/css">
.button-req{
  float: none;
  padding: 0;
  font-size: 16pt;
  font-family: Times;
  width: 500px;
}
</style>
<body>
  <?php
		include "../menu.php";
    include "../registration/session.php";
    include "../database/database-open.php";
	?>
	<div class="main">
	<?php
		$accessory_id = $_POST['accessory_id'];
		$query = 
			"SELECT title,description,price
			FROM accessories
			WHERE id = $accessory_id";
		$result = pg_query($link,$query);
    if($row = pg_fetch_row($result)){?>
      <div class="one-bouquet" align="center">
        <div style="float: left; width: 500px; margin-right: 10px;">
      	<div id="img_popup" style="background: url('/images/flower.png'); background-size: cover;">
      	  <div class="price-popup"><?php echo $row[2]?> &#8381;</div>
      	</div>
        </div>
        <div style="display: block;">
      	<p style="text-align: center; font-size: 30px; text-decoration: underline; padding-bottom: 20px;"><?php echo $row[0]?></p>
      		<?php $row[1] ?>
      	</div>
        <form method="post" action="logic/addAccessoryToBasket.php">
          <input name = "accessory_id" type="hidden" value="<?php echo $accessory_id?>"/>
          <?if(!isset($user_group_id)){?>
            <input class="button-req" style=" margin-top: 10px;" style="width: 500px; margin-top: 10px;" type="submit" value="Добавить в корзину"/>
          <?}?>
        </form>
        <a class="button-req" style="margin-left: 510px;" href="/shop/index.php">Вернуться в магазин</a>
      </div>
    <?php }	?>  

	</div>
  <?php
		include "../footer.php";
		include "../database/database-close.php";
	?>

</body>

</html>