<?php
  include "../../registration/session.php";
  include "../../database/database-open.php";

  if (isset($_POST['bouquet_id'])) {
    $bouquet_id =  $_POST['bouquet_id'];
    $query = 
      "SELECT add_bouquet_to_basket($user_id, $bouquet_id)";
    pg_query($link,$query);
  }

  header("Location: /basket");
  include "../../database/database-close.php";
?>
