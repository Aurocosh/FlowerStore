<?php
  include "../../registration/session.php";
  include "../../database/database-open.php";

  if (isset($_POST['accessory_id'])) {
    $accessory_id =  $_POST['accessory_id'];
    $query = 
      "SELECT add_accessory_to_basket($user_id, $accessory_id)";
    pg_query($link,$query);
  }

  header("Location: /basket");
  include "../../database/database-close.php";
?>
