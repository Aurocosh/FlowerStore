<!DOCTYPE html>
<html lang="ru">

<head>
	<title>Flowery - о букете!</title>
	<meta name="Author" content="author">
	<meta name="Description" content="description">
	<meta name="Keywords" content="keywords">
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="/styles.css">
	<script src="http://code.jquery.com/jquery-latest.js"></script>
</head>
<style type="text/css">
.button-req{
	float: none;
	padding: 0;
	font-size: 16pt;
	font-family: Times;
	width: 500px;
}
</style>

<body>
  <?php
		include "../menu.php";
    include "../registration/session.php";
    include "../database/database-open.php";
	?>
	<div class="main">
		<?php
		$bouquet_id = $_POST['bouquet_id'];
		$query = 
		"SELECT title,description,price
		FROM bouquet_templates
		WHERE id = $bouquet_id";
		$result = pg_query($link,$query);
		if($row = pg_fetch_row($result)){?>
		<div class="one-bouquet">
			<div style="float: left; width: 500px; margin-right: 10px;">
				<div id="img_popup" style="background: url('/images/flower.png'); background-size: cover;">
					<div class="price-popup"><?php echo $row[2]?> &#8381;</div>
				</div>
			</div>
			<div style="display: block;" align="center">
				<p style="text-align: center; font-size: 30px; text-decoration: underline; padding-bottom: 20px;"><?php echo $row[0]?></p>
				<?php $row[1] ?>		

				<!-- Состав шаблона -->
				<?php 
				$query = 
				"(SELECT f.title,
					fibt.count
					FROM bouquet_templates AS bt
					JOIN flowers_in_bouquet_template AS fibt ON fibt.bouquet_template_id = bt.id
					JOIN flowers AS f ON f.id = fibt.flower_id
					WHERE bt.id = '$bouquet_id')
					UNION ALL
					(SELECT a.title,
						aibt.count
						FROM bouquet_templates AS bt
						JOIN accessories_in_bouquet_template AS aibt ON aibt.bouquet_template_id = bt.id
						JOIN accessories AS a ON a.id = aibt.accessory_id
						WHERE bt.id = '$bouquet_id')";
						if ($result = pg_query($link,$query)) {
							while($row = pg_fetch_row($result)){ ?>	
							<div class="busket-list" style="width: 40%;">
								<div class="busket-item" style="width: 90%;"><?=$row[0]?></div>
								<div class="busket-item price_busket" style="width: 10%;"><?=$row[1]?> </div>
							</div>
							<?	}
						} 
						?>
						<form method="post" action="logic/addBouquetToBasket.php">
							<input name = "bouquet_id" type="hidden" value="<?php echo $bouquet_id?>"/>
              <?if(!isset($user_group_id)){?>
                <input class="button-req" style=" margin-top: 10px;" type="submit" value="Добавить в корзину"/>
              <?}?>  
						</form>
						<a class="button-req" style="margin-left: 510px;" href="/shop/index.php">Вернуться в магазин</a>

					</div>
				</div>
			</div>
			<?php }	?>  
			<div style="clear: both;"></div>
			<?php
			include "../footer.php";
			include "../database/database-close.php";
			?>

		</body>

		</html>