<!DOCTYPE html>
<html lang="ru">

<head>
  <title>Flowery - закажи букет!</title>
  <meta name="Author" content="author">
  <meta name="Description" content="description">
  <meta name="Keywords" content="keywords">
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="styles.css">
  <script src="http://code.jquery.com/jquery-latest.js"></script>
</head>

<body>
  <?php
        include "menu.php";
        include "./database/database-open.php";
    ?>

    <div class="main">

      <div style="display: table;">
        <div class="index-cell" style="width: 100%">
          <div>
            <?php
              $query= "SELECT header, description, date FROM news ORDER BY date DESC";
              $result = pg_query($link, $query);
              while ($row = pg_fetch_row($result)){ ?>
              <div class="news">
                <img src="/images/flower.png" />
      
                <div >
                  <h2><?php echo $row[0];?></h2>
                  <hr>
                  <p style="color: gray;">
                    <?php echo $row[2];?>
                  </p>
                  <p>
                    <?php echo $row[1];?>
                  </p>

                </div>
              </div>
              <?php  }  
            ?>
          </div>
        </div>
        <div class="index-cell index-right" style="">
          <?php
            $query='SELECT id, title FROM bouquet_templates ORDER BY purchase_count DESC LIMIT 4';
            
            $result = pg_query($link,$query);
            while($row= pg_fetch_row($result)) { ?>
            <div>
              <img src="/images/flower.png" style="width: 100px;" />
                <p>
                  <form id="bouquet_submit" method="post" action="/shop/bouquet.php">
                    <input name = "bouquet_id" type="hidden" value="<?php echo $row[0]?>" />
                    <a href="" onClick="this.parentNode.submit(); return false;"><?php echo $row[1]?></a>
                  </form>
                </p>
            </div>  
          <?php }?>
        </div>
      </div>

    </div>

    <?php
        include "footer.php";
        include "./database/database-close.php";
    ?>

</body>

</html>