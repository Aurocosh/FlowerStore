<!DOCTYPE html>
<html lang="ru">

<head>
  <title>Flowery - управление работниками</title>
  <meta name="Author" content="author">
  <meta name="Description" content="description">
  <meta name="Keywords" content="keywords">
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="/styles.css">
  <script src="/js/jquery.min.js"></script>
  <script src="/js/index.js"></script>
  <script>  
        function deleteItemFromBusket(id)  
        {  
            $.ajax({  
                url: "./logic/delete-employee.php", 
				data: "idItem="+id,			
                cache: false,  
                success: function(html){  
                    $("#employeesContent").html(html);  
                }  
            });  
        }     
  </script>
</head>
<style>
.button_inp{
    text-decoration: none; 
    position: relative;
    display: block;
    float: right;
    width: 100px;
    height: 30px;
    padding: 0;
    margin: 0;
    font-family: Times;
    font-size: 12pt;
    color: white;
    text-align: center;
    text-shadow: 0 1px 2px rgba(0, 0, 0, 0.25);
    background: #B1608E;
    border: 1px solid #764E80;
    cursor: pointer;
  }
</style>

<body>
  <?php
	include "../menu.php";
	include "../database/database-open.php";
  ?>
  <div class="main" style="padding-bottom: 400px;">
    <div class="admin-style">
      <h2>Добавить сотрудника</h2>
      <form method="post" action="logic/addNewEmployee.php">
        <input name="name" type="text" value="Имя" />
        <input name="lastname" type="text" value="Фамилия" />
        <input name="patronymic" type="text" value="Отчество" />
        <input name="email" type="email" value="E-mail" />
        <input name="password" type="text" value="Password" />
        <select name="type" style="margin-right: 20px; width: 150px; height: 40px; font-size: 20px;">
            <option value="1">Админ</option>
            <option value="2">Менеджер</option>
            <option value="3">Флорист</option>
          </select>

        <input class="button_inp" type="submit" value="Добавить" />
      </form>
      <h2>Удалить сотрудника</h2>
	  <div id="employeesContent">
		  <?php
				$query = 
					"SELECT e.id,
						concat_ws(' ', e.lastname,e.firstname,e.patronymic),
						e.email,
						ug.title
					FROM employees AS e
					JOIN user_groups AS ug ON ug.id = e.user_group_id";
				if ($result = pg_query($link,$query)) {
					while($row = pg_fetch_row($result)){ ?>
						<div class="busket-list" id="<?=$row[0]?>">
              <div class="busket-item" style="width: 40%;"><?=$row[1]?></div>
							<div class="busket-item" style="width: 10%;"><?=$row[3]?></div>
							<div class="busket-item" style="width: 10%;"><?=$row[2]?></div>
							<div class="busket-item" style="width: 10%;"><a id="<?=$row[0]?>" href="delete.html?id=2" target="_blank" onclick="return confirmDelete(this);" class="delete-link need-to-confirm">
							  <span class="maintext">&#10008; Удалить</span>
							  <span class="confirmation">&#10004; Уверены?</span>
							</a></div>
						</div>
					<?}
				}
			?>
		</div>
    </div>
  </div>
  <div style="clear: both;"></div>
    <?php
		include "../footer.php";
		include "../database/database-close.php";
	?>
</body>

</html>