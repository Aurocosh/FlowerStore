<!DOCTYPE html>
<html lang="ru">

<head>
  <title>Flowery - отчетность</title>
  <meta name="Author" content="author">
  <meta name="Description" content="description">
  <meta name="Keywords" content="keywords">
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="/styles.css">
  <script src="/js/jquery.min.js"></script>
  <script src="/js/index.js"></script>
  <script>  
      function one(file) {
        $.ajax({  
          url: file+".php",      
          cache: false,  
          success: function(html){  
            $("#info").html(html);  
          }  
        });  
      }
  </script>
</head>
<style>
button {
  width: 300px;
}
</style>

<body>
  <?php
	include $_SERVER['DOCUMENT_ROOT']."/menu.php";
	include $_SERVER['DOCUMENT_ROOT']."/database/database-open.php";
  ?>
  <div class="main">
    <button onclick="one('one')">Время сборки букетоа</button> 
    <button onclick="one('two')">Список заданий флористов</button> 
    <button onclick="one('three')">Букеты заказанные клиентами</button> 
    <button onclick="one('four')">Букеты заказанные менеджерами</button> 
    <button onclick="one('five')">Букеты для массовых мероприятий</button> 
    <button onclick="one('six')">Полная статистика по букетам</button> 
    <button onclick="one('seven')">Список клиентов</button> 
    <button onclick="one('eight')">Остаток материалов на складе</button> 
    <button onclick="one('nine')">Потраченные материалы</button> 
    <button onclick="one('ten')">Полученные материалы</button>
    
    <div id="info" style="margin-top: 100px;">
      
    </div>
  </div>
  <div style="clear: both;"></div>
    <?php
		include $_SERVER['DOCUMENT_ROOT']."/footer.php";
		include $_SERVER['DOCUMENT_ROOT']."/database/database-close.php";
	?>
</body>

</html>