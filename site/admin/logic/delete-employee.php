﻿<?php 
	include "../../database/database-open.php";
	
	$idEmployee = $_REQUEST['idItem'];
	
	$query = "DELETE FROM employees WHERE id = $idEmployee";
	pg_query($link, $query);
	
	$query = 
		"SELECT e.id,
			concat_ws(' ', e.lastname,e.firstname,e.patronymic),
			e.email,
			ug.title
		FROM employees AS e
		JOIN user_groups AS ug ON ug.id = e.user_group_id";
	if ($result = pg_query($link,$query)) {
		while($row = pg_fetch_row($result)){
			echo "<div class=\"busket-list\" id=\"$row[0]\">";
			echo "	<div class=\"busket-item\" style=\"width: 100%;\">$row[1]</div>";
			echo "	<div class=\"busket-item\">$row[3]</div>";
			echo "	<div class=\"busket-item\">$row[2]</div>";
			echo "	<div class=\"busket-item\"><a id=\"$row[0]\" href=\"delete.html?id=2\" target=\"_blank\" onclick=\"return confirmDelete(this);\" class=\"delete-link need-to-confirm\">";
			echo "	  <span class=\"maintext\">&#10008; Удалить</span>";
			echo "	  <span class=\"confirmation\">&#10004; Уверены?</span>";
			echo "	</a></div>";
			echo "</div>";
		}
	}
	
	include "../../database/database-close.php";
?>