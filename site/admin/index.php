<!DOCTYPE html>
<html lang="ru">

<head>
  <title>Flowery - личный кабинет администратора</title>
  <meta name="Author" content="author">
  <meta name="Description" content="description">
  <meta name="Keywords" content="keywords">
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="/styles.css">
  <script src="/js/jquery.min.js"></script>
  <script src="/js/index.js"></script>
  <script>  
        function deleteItemFromBusket(id)  
        {  
            $.ajax({  
                url: "./logic/delete-employee.php", 
				data: "idItem="+id,			
                cache: false,  
                success: function(html){  
                    $("#employeesContent").html(html);  
                }  
            });  
        }     
  </script>
</head>
<style>
  .button_a{
    text-decoration: none; 
    position: relative;
    display: block;
    float: left;
    width: 30%;
    height: 40px;
    padding: 0;
    margin: 0;
    padding-top: 10px;
    font-family: Times;
    font-size: 14pt;
    color: white;
    text-align: center;
    text-shadow: 0 1px 2px rgba(0, 0, 0, 0.25);
    background: #B1608E;
    border: 1px solid #764E80;
    cursor: pointer;
  }

  .hhh{
    padding-top: 0px;
    height: 52px;
  }
</style>

<body>
  <?php
	include "../menu.php";
	include "../database/database-open.php";
  ?>
  <div class="main">
    <a class="button_a" href="employees.php">Управление сотрудниками</a>
    <a class="button_a" href="files">Доступ к отчетности</a>
    <form method="post" action="/registration/authorization.php">  
        <input id="closeButton" type="submit" name="closeButton" class="button_a hhh" value="Выйти"></input>
      </form>
  </div>
  <div style="clear: both;"></div>
    <?php
		include "../footer.php";
		include "../database/database-close.php";
	?>
</body>

</html>