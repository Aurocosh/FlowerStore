<!DOCTYPE html>
<html lang="ru">

<head>
  <title>Flowery - 404</title>
  <meta name="Author" content="author">
  <meta name="Description" content="description">
  <meta name="Keywords" content="keywords">
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="styles.css">
  <script src="js/jquery.min.js"></script>
  <script src="js/index.js"></script>
  <script>
  </script>
</head>
<style>
.content {
/*  padding-top: 100px;*/
  width:100%;
  text-align:center;
  position:absolute;
  bottom:10%;
  left:0px;}
.content a {display:inline-block;text-decoration:none}
.content a:hover {opacity:0.7}
.content a, .content a:hover {color:#000;}
}
</style>

<body>
  <?php
    include "menu.php";
    include "./database/database-open.php";
  ?>
  <div class="main">
  
  <div class="content">
    <img style="padding-bottom: 20px" src="images/404.png" /> <br>
	<a href="/">Перейти к главной странице</a>
  </div>
  </div>
  <div style="clear: both;"></div>
    <?php
		include "footer.php";
		include "./database/database-close.php";
	?>
</body>

</html>