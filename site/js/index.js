

window.confirmDelete = (function (element) {
  var className = element.className;

  if (className.indexOf('need-to-confirm') > -1) {
    element.className = className.replace('need-to-confirm', 'confirmed');
    return false;
  } else {
    deleteItemFromBusket(element.id);
	return false;
  }
});

function toggle_florist(obj, el) {
      el.style.display = (el.style.display == 'none') ? '' : 'none';
      if (obj.innerHTML == 'Взять заказ') {
        obj.innerHTML = 'Завершить работу';
      } else {
        obj.innerHTML = 'Взять заказ';
      }

    }