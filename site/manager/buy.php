<!DOCTYPE html>
<html lang="ru">

<head>
  <title>Flowery - заказ поставки</title>
  <meta name="Author" content="author">
  <meta name="Description" content="description">
  <meta name="Keywords" content="keywords">
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="/styles.css">
  <script src="/js/jquery.min.js"></script>
  <script src="/js/index.js"></script>
  <script>

    $(document).ready(function() {
      $.ajax({
        url: "./logic/showMaterials.php",
        cache: false,
        success: function(html) {
          $("#materialsContent").html(html);
        }
      });
    });
  </script>
</head>
<style>
.filter_button{
  float: right;
  font-size: 18px;
  width: 100%;
  padding: 0;
  margin-bottom: 10px;
  margin-top: 10px;
}

h2 {
  margin-bottom: 10px;
}
</style>

<body>
  <?php
  include "../menu.php";
  include "../registration/session.php";
  include "../database/database-open.php";
  ?>
  <div class="main" align="center">

  <form id="formMaterials" name="form" action="/manager/logic/orderMaterials.php" method="post">
    <div id="materialsContent">

    </div>

    <div style="font-size: 18px;">
      <!-- <h2>Уточнения о доставке</h2> -->
		<input type="date" name="plannedDate" value=""/>
        <input type="submit" class="busket-button" name="orderButton" value="Заказать"></input>
    </div>
  </form> 


  </div>
  <div style="clear: both;"></div>
  <script>
  </script>
  <?php
  include "../footer.php";
  include "../database/database-close.php";
  ?>
</body>

</html>