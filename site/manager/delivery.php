<!DOCTYPE html>
<html lang="ru">

<head>
  <title>Flowery - доставка</title>
  <meta name="Author" content="author">
  <meta name="Description" content="description">
  <meta name="Keywords" content="keywords">
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="/styles.css">
  <script src="/js/jquery.min.js"></script>
  <script src="/js/index.js"></script>
  <script>
    function markDelivery(id) {
      $.ajax({
        url: "/manager/markDelivery.php",
        data: "idItem=" + id,
        cache: false,
        success: function(html) {
          $.ajax({
        url: "/manager/deliveryContent.php",
        cache: false,
        success: function(html) {
          $("#deliveryContent").html(html);
        }
      });
        }
      });
    }
    
    $(document).ready(function() {
      $.ajax({
        url: "/manager/deliveryContent.php",
        cache: false,
        success: function(html) {
          $("#deliveryContent").html(html);
        }
      });
    });

    
  </script>
</head>
<style>
  .filter_button {
    width: 100px;
  }
  .h2 {
  margin-bottom: 10px;
}
</style>

<body>
  <?php
  include "../menu.php";
  include "../registration/session.php";
  include "../database/database-open.php";
  ?>
  <div class="main" align="center">
    <h2>Доставка</h2>
    <div id="deliveryContent">

    </div>
  </div>
  <div style="clear: both;"></div>
  <script>
  </script>
  <?php
  include "../footer.php";
  include "../database/database-close.php";
  ?>
</body>

</html>