<!DOCTYPE html>
<html lang="ru">

<head>
  <title>Flowery - добавление аксессуара</title>
  <meta name="Author" content="author">
  <meta name="Description" content="description">
  <meta name="Keywords" content="keywords">
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="/styles.css">
  <script src="/js/jquery.min.js"></script>
  <script src="/js/index.js"></script>
</head>
<style>
.button_a{
  float: none;
  font-size: 18px;
  width: 200px;
  padding: 0;
}
p {
  height: 30px;
}
h2 {
  margin-bottom: 10px;
}
</style>

<body>
  <?php
	include "../menu.php";
	include "../database/database-open.php";
  ?>
  <div class="main" align="center">

    <h2>Добавить новый акссессуар</h2>
      
      <div id="accessory">
        <form method = "post" action="logic/addNewAccessory.php">
          <p><input name = "name" type="text" value="Название" /></p>
          <p><input name = "price" type="number" value="100" /> </p>
          <p style="font-size: 16pt;">Доступен к продаже <input name = "available" type="checkbox" value="true"/> </p>
          <input class="button_a" type="submit" value="Добавить" />
        </form>
      </div>

  </div>
  <div style="clear: both;"></div>
    <?php
		include "../footer.php";
		include "../database/database-close.php";
	?>
</body>

</html>