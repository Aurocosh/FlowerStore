<?
include $_SERVER['DOCUMENT_ROOT']."/database/database-open.php";
include $_SERVER['DOCUMENT_ROOT']."/registration/session.php";

$query = 
  "SELECT bt.id,
      bte.title,
      concat_ws(' ',c.lastname,c.firstname,c.patronymic),
      c.address,
      c.phone,
      p.delivery
  FROM bouquet_tasks AS bt
  JOIN bouquets AS b ON b.id = bt.bouquet_id
  JOIN bouquet_templates AS bte ON bte.id = b.bouquet_template_id
  JOIN purchases AS p ON p.id = b.purchase_id
  JOIN clients AS c ON c.id = p.client_id
  WHERE bouquet_status_id = 2
      AND p.client_id IS NOT NULL
      AND p.delivery IS NOT NULL
      AND p.nulled IS NULL";

  $result = pg_query($link,$query);?>
<div class="busket-list">
        <div class="busket-item" style="width: 10%;">Букет</div>
        <div class="busket-item" style="width: 20%;">Клиент</div>
        <div class="busket-item" style="width: 10%;">Адрес</div>
        <div class="busket-item" style="width: 10%;">Телефон</div>
        <div class="busket-item" style="width: 10%;">Дата доставки</div>
        <div class="busket-item" style="width: 6%"></div>
      </div>
 <? while($row = pg_fetch_row($result)){ ?>
			<div class="busket-list">
				<div class="busket-item" style="width: 10%;"><?=$row[1]?></div>
				<div class="busket-item" style="width: 20%;"><?=$row[2]?></div>
				<div class="busket-item" style="width: 10%;"><?=$row[3]?></div>
				<div class="busket-item" style="width: 10%;"><?=$row[4]?></div>
				<div class="busket-item" style="width: 10%;"><?=$row[5]?></div>
				<div class="busket-item" style="width: 6%">
          <input type=button class="filter_button" onclick="markDelivery(<?=$row[0]?>);" value="Доставлено" >
        </div>
			</div>
<?
	}
	include $_SERVER['DOCUMENT_ROOT']."/database/database-close.php";
?>