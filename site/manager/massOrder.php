<!DOCTYPE html>
<html lang="ru">

<head>
  <title>Flowery - массовый заказ</title>
  <meta name="Author" content="author">
  <meta name="Description" content="description">
  <meta name="Keywords" content="keywords">
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="/styles.css">
  <script src="/js/jquery.min.js"></script>
  <script src="/js/index.js"></script>
</head>
<style>
.filter_button{
  width: 100px;
}
.h2 {
  margin-bottom: 10px;
}
</style>

<body>
  <?php
  include "../menu.php";
  include "../registration/session.php";
  include "../database/database-open.php";
  ?>
  <div class="main">
    <h2>Массовые заказы</h2>
    <div id="busketContent">
      <?
      $query = 
        "SELECT id
        FROM requests
        WHERE employee_id = 0";
      $result = pg_query($link,$query);
      if($row = pg_fetch_row($result)){
        header("Location: ../newtemplate.php");
      }
      
      $query = 
        "SELECT r.id,
            r.date,
            concat_ws(' ',c.lastname,c.firstname,c.patronymic) AS fio,
            c.email,
            c.address,
            c.phone
        FROM requests AS r
        JOIN clients AS c ON c.id = r.client_id
        WHERE r.employee_id IS NULL
        ORDER BY r.date,
            fio";
      $result = pg_query($link,$query);?>
      <div class="busket-list">
          <div class="busket-item" style="width: 5%;">Дата</div>
          <div class="busket-item" style="width: 20%;">Клиент</div>
          <div class="busket-item" style="width: 15%;">E-mail</div>
          <div class="busket-item" style="width: 10%;">Адрес</div>
          <div class="busket-item" style="width: 10%;">Телефон</div>
          <div class="busket-item" style="width: 6%;"></div>
        </div>
      <?while($row = pg_fetch_row($result)){ ?>
        <div class="busket-list">
          <div class="busket-item" style="width: 5%;"><?=$row[1]?></div>
          <div class="busket-item" style="width: 20%;"><?=$row[2]?></div>
          <div class="busket-item" style="width: 15%;"><?=$row[3]?></div>
          <div class="busket-item" style="width: 10%;"><?=$row[4]?></div>
          <div class="busket-item" style="width: 10%;"><?=$row[5]?></div>
          <div class="busket-item" style="width: 6%;">
            <form method="post" action="logic/takeRequest.php">
              <input type="hidden" name="request_id" value="<?=$row[0]?>"></input>
              <input class="filter_button" type="submit" value="Обработать"></input>
            </form> 
          </div>
        </div>
    <?}?>
    </div>
  </div>
  <div style="clear: both;"></div>
  <?php
  include "../footer.php";
  include "../database/database-close.php";
  ?>
</body>

</html>