<?
	include $_SERVER['DOCUMENT_ROOT']."/database/database-open.php";
	include $_SERVER['DOCUMENT_ROOT']."/registration/session.php";
?>

	<h2>Поставки</h2>
		<?
		$query = "SELECT supplies.id,supplies.title,supplies.created, supplies.planned, supplies.received, ss.title FROM supplies
		JOIN supply_statuses AS ss ON ss.id = supplies.supply_status_id order by created";

		if ($result = pg_query($link,$query)) {
			while($row = pg_fetch_row($result)){ ?>
			<div class="busket-list" id="<?=$row[0]?>" itemType="<?=$row[1]?>">
				<input type="hidden" name="supplies[]" value="<?=$row[0]?>">
				<div class="busket-item" style="width: 40%;"><?=$row[1]?></div>
				<div class="busket-item" style="width: 10%;"><?=$row[2]?></div>
				<div class="busket-item" style="width: 10%;"><?=$row[3]?></div>
				<div class="busket-item" style="width: 10%;"><?if(isset($row[4])) echo $row[4]; else echo '----'?></div>
				<div class="busket-item" style="width: 10%;"><?=$row[5]?></div>
				<div class="busket-item" style="width: 10%;"><button  style="width: 100%;" name="Accept" value="3 <?=$row[0]?>" 
				onclick="return changeStatus(this);">Получено</button></div>
				<div class="busket-item" style="width: 10%;"><button  style="width: 100%;" name="Cancel" value="2 <?=$row[0]?>" 
				onclick="return changeStatus(this);">Отменить</button></div>
			</div>
		<?	}
		}
		?>

	<h2>Цветы</h2>
	<?
	$query = "SELECT id,title from flowers";

	if ($result = pg_query($link,$query)) {
		while($row = pg_fetch_row($result)){ ?>
		<div class="busket-list" id="<?=$row[0]?>" itemType="<?=$row[1]?>">
			<div class="busket-item" style="width: 3%;"><img style="width: 50px" src="/images/flower.png" /></div>
			<div class="busket-item" style="width: 40%;">
				<?=$row[1]?>
			</div>
			<input type="hidden" name="flowersId[]" value="<?=$row[0]?>">
			<div class="busket-item" style="width: 4%;>
				<div class="amount">
					<span class="down">-</span>
					<input type="text" class="busket_count" name="flowersCount[]"  value="0" />
					<span class="up">+</span>
				</div>
			</div>
		</div>
	<?	}
	}
	?>
	<br><br><h2>Аксессуары</h2>
	<?
	
	$query = "SELECT id,title from accessories";

	if ($result = pg_query($link,$query)) {
		while($row = pg_fetch_row($result)){ ?>
		<div class="busket-list" id="<?=$row[0]?>" itemType="<?=$row[1]?>">
			<div class="busket-item" style="width: 4%;"><img style="width: 50px" src="/images/flower.png" /></div>
			<div class="busket-item" style="width: 40%;">
				<?=$row[1]?>
			<input type="hidden" name="accesId[]" value="<?=$row[0]?>">
			</div>
			<div class="busket-item" style="width: 4%;">
				<div class="amount">
					<span class="down">-</span>
					<input type="text" class="busket_count" name="accesCount[]" value="0" />
					<span class="up">+</span>
				</div>
			</div>
		</div>
	<?	}
	}
	?>
	
<?
	include $_SERVER['DOCUMENT_ROOT']."/database/database-close.php";
?>

<script type="text/javascript">
	$(document).ready(function () {
      $('.down').click(function () {
        var $input = $(this).parent().find('input');
        var count = parseInt($input.val()) - 1;
        count = count < 1 ? 1 : count;
        $input.val(count);
        $input.change();

        return false;
      });
      $('.up').click(function () {
        var $input = $(this).parent().find('input');
        $input.val(parseInt($input.val()) + 1);
        $input.change();

        return false;
      });
    });
	
	function changeStatus(id) {
		$.ajax({
        url: "./logic/changeSuppliesStatus.php",
        data: "idItem=" + id.value,
        cache: false,
        success: function(html) {
          $.ajax({
        url: "./logic/showMaterials.php",
        cache: false,
        success: function(html) {
          $("#materialsContent").html(html);
        }
      });
        }
      });
		return false;
    }

    
</script>