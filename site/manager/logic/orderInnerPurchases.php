<?php
  include "../../registration/session.php";
  include "../../database/database-open.php";
  
  if (isset($_POST['orderButton'])) {
	  $Bcount = $_POST['Bcount'];
	  $Bid = $_POST['Bid'];
	  
	  
	  $f = false;
	  for ($i=0; $i<count($Bcount);$i++) {
		if ($Bcount[$i] > 0)  
			$f = true;
	  }
	  
	  if($f) {
		  $query = "INSERT INTO purchases(employee_id,purchase_type_id)
		  VALUES ($user_id,3)
		  RETURNING id";
		  if ($result = pg_query($link, $query)) {
				if($row = pg_fetch_row($result)) {
					$purchase_id = $row[0];
					for ($i=0; $i<count($Bid);$i++) {
						if ($Bcount[$i] > 0) {
							$bouquet_template_id = $Bid[$i];
							$count = $Bcount[$i];
							$query = 
							"INSERT INTO bouquets(bouquet_template_id,purchase_id,price,count)
							SELECT bt.id,'$purchase_id',bt.price,'$count'
							FROM bouquet_templates AS bt
							WHERE bt.id = '$bouquet_template_id'
							RETURNING id";							
							$result = pg_query($link, $query);
							
							$row = pg_fetch_row($result);
							$bouquet_id = $row[0];

							$query = 
								"INSERT INTO flowers_in_bouquet(bouquet_id,flower_id,count)
								SELECT '$bouquet_id',flower_id,count
								FROM flowers_in_bouquet_template
								WHERE bouquet_template_id = (SELECT bouquet_template_id FROM bouquets WHERE id = '$bouquet_id')";
							$result = pg_query($link, $query);
							$query = 
								"INSERT INTO accessories_in_bouquet(bouquet_id,accessory_id,count)
								SELECT $bouquet_id,accessory_id,count
								FROM accessories_in_bouquet_template
								WHERE bouquet_template_id = (SELECT bouquet_template_id FROM bouquets WHERE id = '$bouquet_id')";
							$result = pg_query($link, $query);
						} 
					}					
				}
		  }
	  }
	  
  }
  
  header("Location: ../innerPurchases.php");
  include "../../database/database-close.php";
?>