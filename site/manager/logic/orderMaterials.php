<?php
  include "../../registration/session.php";
  include "../../database/database-open.php";
  
  if (isset($_POST['orderButton'])) {
	  $flowersCount = $_POST['flowersCount'];
	  $flowersId = $_POST['flowersId'];
	  $accesCount = $_POST['accesCount'];
	  $accesId = $_POST['accesId'];
	  $plannedDate = $_POST['plannedDate'];
	  echo "<h1>".$flowersCount[1]." ".$flowersId[1]."</h1>";
	  $title = "Поставка ".date('j-m-y');
	  
	  $f = false;
	  for ($i=0; $i<count($flowersCount);$i++) {
		if ($flowersCount[$i] > 0)  
			$f = true;
	  }
	  for ($i=0; $i<count($accesCount);$i++) {
		if ($accesCount[$i] > 0)  
			$f = true;
	  }
	  
	  if($f) {
		  $query = 
		  "INSERT INTO supplies(title, planned,supply_status_id)
		  VALUES('$title','$plannedDate', 1)
		  RETURNING id";
		  if ($result = pg_query($link, $query)) {
				if($row = pg_fetch_row($result)) {
					$query1 = " ";
					for ($i=0; $i<count($flowersId);$i++) {
						if ($flowersCount[$i] > 0) {
							$query1 = $query1."INSERT INTO supply_flower_batch(supply_id, flower_id,count)
							VALUES('$row[0]','$flowersId[$i]', '$flowersCount[$i]');";
						}
					}
					
					for ($i=0; $i<count($accesId);$i++) {
						if ($accesCount[$i] > 0) {
							$query1 = $query1."INSERT INTO supply_accessory_batch(supply_id, accessory_id,count)
							VALUES('$row[0]','$accesId[$i]','$accesCount[$i]');";
						}
					}
					//echo $query1;
					pg_query($link, $query1);
				}
		  }
	  }
	  
  }
  
  header("Location: ../buy.php");
  include "../../database/database-close.php";
?>