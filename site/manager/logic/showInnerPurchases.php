<?
	include $_SERVER['DOCUMENT_ROOT']."/database/database-open.php";
	include $_SERVER['DOCUMENT_ROOT']."/registration/session.php";
?>

	<h2>Виды букетов</h2>
	<?
	$query = "SELECT id,title from flowers";

	if ($result = pg_query($link,$query)) {
		while($row = pg_fetch_row($result)){ ?>
		<div class="busket-list" id="<?=$row[0]?>" itemType="<?=$row[1]?>">
			<div class="busket-item" style="width: 3%;"><img style="width: 50px" src="/images/flower.png" /></div>
			<div class="busket-item" style="width: 40%;">
				<?=$row[1]?>
			</div>
			<input type="hidden" name="Bid[]" value="<?=$row[0]?>">
			<div class="busket-item" style="width: 4%;">
				<div class="amount">
					<span class="down">-</span>
					<input type="text" class="busket_count" name="Bcount[]"  value="0" />
					<span class="up">+</span>
				</div>
			</div>
		</div>
	<?	}
	}
	?>
		
<?
	include $_SERVER['DOCUMENT_ROOT']."/database/database-close.php";
?>

<script type="text/javascript">
	$(document).ready(function () {
      $('.down').click(function () {
        var $input = $(this).parent().find('input');
        var count = parseInt($input.val()) - 1;
        count = count < 1 ? 1 : count;
        $input.val(count);
        $input.change();

        return false;
      });
      $('.up').click(function () {
        var $input = $(this).parent().find('input');
        $input.val(parseInt($input.val()) + 1);
        $input.change();

        return false;
      });
    });

    
</script>