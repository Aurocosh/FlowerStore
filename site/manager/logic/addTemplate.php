<?php
  include "../../registration/session.php";
  include "../../database/database-open.php";
  
  $template_name = isset($_POST['template_name']) ? $_POST['template_name'] : '';
  $template_price = isset($_POST['template_price']) ? $_POST['template_price'] : '';
    
  $query = 
    "INSERT INTO bouquet_templates(title,price)
    VALUES ('$template_name',$template_price)
    RETURNING id";		
  if ($result = pg_query($link,$query)) {
    if($row = pg_fetch_row($result)){
      $template_id = $row[0];
      
      $query = 
        "INSERT INTO flowers_in_bouquet_template(bouquet_template_id,flower_id,count)
        SELECT $template_id,flower_id,count
        FROM flowers_in_new_bouquet_template
        WHERE employee_id = $user_id";
      pg_query($link,$query);
      
      $query = 
        "INSERT INTO accessories_in_bouquet_template(bouquet_template_id,accessory_id,count)
        SELECT $template_id,accessory_id,count
        FROM accessories_in_new_bouquet_template
        WHERE employee_id = $user_id";
      pg_query($link,$query);
      
      $query = 
        "DELETE FROM flowers_in_new_bouquet_template
        WHERE employee_id = $user_id";
      pg_query($link,$query);
      
      $query = 
        "DELETE FROM accessories_in_new_bouquet_template
        WHERE employee_id = $user_id";
      pg_query($link,$query);
    }
  }
  
  header("Location: /shop/index.php");
  include "../../database/database-close.php";
?>