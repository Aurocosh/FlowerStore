<!DOCTYPE html>
<html lang="ru">

<head>
  <title>Flowery - закажи букет!</title>
  <meta name="Author" content="author">
  <meta name="Description" content="description">
  <meta name="Keywords" content="keywords">
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="/styles.css">
  <script src="/js/jquery.min.js"></script>
  <script src="/js/index.js"></script>
  <script>

    $(document).ready(function() {
		$('.down').click(function () {
        var $input = $(this).parent().find('input');
        var count = parseInt($input.val()) - 1;
        count = count < 1 ? 1 : count;
        $input.val(count);
        $input.change();

        return false;
      });
      $('.up').click(function () {
        var $input = $(this).parent().find('input');
        $input.val(parseInt($input.val()) + 1);
        $input.change();

        return false;
      });
    });
  </script>
</head>
<style>
.filter_button{
  float: right;
  font-size: 18px;
  width: 100%;
  padding: 0;
  margin-bottom: 10px;
  margin-top: 10px;
}

h2 {
  margin-bottom: 10px;
}
</style>

<body>
  <?php
  include "../../menu.php";
  include "../../registration/session.php";
  include "../../database/database-open.php";
  
	if (isset($_POST['request_id'])) {
		$request_id =  $_POST['request_id'];
	}
  ?>
  <div class="main" align="center">

  <form id="formMaterials" name="form" action="/manager/logic/orderRequest.php" method="post">
    <div id="materialsContent">
	<h2>Виды букетов</h2>
	<?
	$query = "SELECT id,title from bouquet_templates";

	if ($result = pg_query($link,$query)) {
		while($row = pg_fetch_row($result)){ ?>
		<div class="busket-list" id="<?=$row[0]?>" itemType="<?=$row[1]?>">
			<div class="busket-item" style="width: 3%;"><img style="width: 50px" src="/images/flower.png" /></div>
			<div class="busket-item" style="width: 40%;">
				<?=$row[1]?>
			</div>
			<input type="hidden" name="Bid[]" value="<?=$row[0]?>">
			<div class="busket-item" style="width: 4%;">
				<div class="amount">
					<span class="down">-</span>
					<input type="text" class="busket_count" name="Bcount[]"  value="0" />
					<span class="up">+</span>
				</div>
			</div>
      </div>
	<?	}
	}
	?>
    

    <div style="font-size: 18px;">
      <!-- <h2>Уточнения о доставке</h2> -->
		<input type="hidden" name="request_id" value="<?=$request_id?>">
        <input type="submit" class="filter_button" name="orderButton" value="Заказать"></input>
    </div>
  </form> 


  </div>
  <div style="clear: both;"></div>
  <script>
  </script>
  <?php
  include "../../footer.php";
  include "../../database/database-close.php";
  ?>
</body>

</html>