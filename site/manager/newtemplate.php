<!DOCTYPE html>
<html lang="ru">

<head>
  <title>Flowery - добавление букета</title>
  <meta name="Author" content="author">
  <meta name="Description" content="description">
  <meta name="Keywords" content="keywords">
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="/styles.css">
  <script src="/js/jquery.min.js"></script>
  <script src="/js/index.js"></script>

</head>
<style>
	button, .filter_button{
	  float: left;
	  font-size: 18px;
	  width: 50%;
	  padding: 0;
	  margin-bottom: 10px;
	  margin-top: 10px;
	}

	h2 {
	  margin-bottom: 10px;
	}
</style>
<script type="text/javascript">
	function openWin(id) {
		var t = window.open('','Новое окно','width=800,height=300').document;
    	t.open();
    	if (id===1) 
    		var db = "FLOWERS";
    	else
    		var db = "accessories";
    	$.ajax({
	        url: "logic/flowerList.php",
	        data: {i: db,
	        	type: id},
	        cache: false,
	        success: function(data) {
		    	t.write(data);
	        }
	    });

    	t.close();
    	// window.location.reload();
	}
</script>

<body>
  <?php
    include "../menu.php";
    include "../registration/session.php";
    include "../database/database-open.php";
  ?>
  <div class="main" align="center">
	
	<h2>Добавить новый шаблон букета</h2>
	<div id="template">
		<!-- <form method="post" action="logic/addTemplate.php"> -->
			<form method="post" action="">
			<input type="text" name="template_name" id="name" value="Название" onfocus="if(this.value==this.defaultValue)this.value='';" onblur="if(this.value=='')this.value=this.defaultValue; save(this)" />
            <input type="number" name="template_price" id="price" value="100" onfocus="if(this.value==this.defaultValue)this.value='';" onblur="if(this.value=='')this.value=this.defaultValue; save(this)"/> <br/>
			<button onclick="openWin(1); return false;" >Добавить цветок</button> <button onclick="openWin(2); return false;">Добавить акссесуар</button><br><br>

			<?php
		  
				$query = 
					"SELECT fnb.id,f.title,fnb.count
					FROM flowers_in_new_bouquet_template AS fnb
					JOIN flowers AS f ON f.id = fnb.flower_id
					WHERE fnb.employee_id = $user_id";

				if ($result = pg_query($link,$query)) {
					while($row = pg_fetch_row($result)){	?>					
						<div class="busket-list" id="<?=$row[0]?>">
							<div class="busket-item" style="width: 40%; text-align: center;"><?=$row[1]?></div>
							<div class="busket-item" style="width: 5%; text-align: center;">
									<div class="amount" >
										<span class="down">-</span>
											<input type=text title="<?=$row[0].'_0'?>" value="<?=$row[2]?>"/>
										<span class="up">+</span>
									</div>
								</div>
						</div>
				<?	}
				}
				
				
				
				$query = 
					"SELECT anb.id,a.title,anb.count
					FROM accessories_in_new_bouquet_template AS anb
					JOIN accessories AS a ON a.id = anb.accessory_id
					WHERE anb.employee_id = $user_id";

				if ($result = pg_query($link,$query)) {
					while($row = pg_fetch_row($result)){ ?>						
						<div class="busket-list" id="<?=$row[0]?>">
							<div class="busket-item" style="width: 40%; text-align: center;"><?=$row[1]?></div>
							<div class="busket-item" style="width: 5%; text-align: center;">
									<div class="amount">
										<span class="down">-</span>
											<input type=text title="<?=$row[0].'_1'?>" value="<?=$row[2]?>"/>
										<span class="up">+</span>
									</div>
								</div>
						</div>
					<?}
				}
			?>
          <input class="filter_button" type="submit" value="Добавить" onclick="clearLocal()"/>
		</form>
		<form method="post" action="logic/cancelCreatingOfTemplate.php">
		  <input class="filter_button" type="submit" value="Отменить" onclick="clearLocal()"/>
		</form>
	</div>
  </div>

  

  <div style="clear: both;"></div>
    <?php
		include "../footer.php";
		include "../database/database-close.php";
	?>
  
  <script>
    if (localStorage.length>0) {
    if (localStorage.getItem('name').length > 0)
      document.getElementById('name').value = localStorage.getItem('name');
    if (localStorage.getItem('price').length > 0)
      document.getElementById('price').value = localStorage.getItem('price');
      }
    function save(object) {
      
      localStorage.setItem(object.id, object.value);
    }
    function clearLocal() {
      localStorage.clear();
    }
	
	$(document).ready(function () {
	  $('.down').click(function () {
		var $input = $(this).parent().find('input');
		var count = parseInt($input.val()) - 1;
		count = count < 1 ? 1 : count;
		$input.val(count);
		$input.change();
		var temp, attr, id;
		temp = $input.attr('title').indexOf('_');
		id = parseInt($input.attr('title').substring(0,temp));
		attr = parseInt($input.attr('title').substring(temp+1));
	$.ajax({
		url: "/manager/increment.php",
		data: {id: id, attr: attr, count: $input.val()},
		cache: false,
	  });

		return false;
	  });
	  $('.up').click(function () {
		var $input = $(this).parent().find('input');
		$input.val(parseInt($input.val()) + 1);
		var temp, attr, id;
		temp = $input.attr('title').indexOf('_');
		id = parseInt($input.attr('title').substring(0,temp));
		attr = parseInt($input.attr('title').substring(temp+1));
		$input.change();

	$.ajax({
		url: "/manager/increment.php",
		data: {id: id, attr: attr, count: $input.val()},
		cache: false,
	  });

		return false;
	  });
	});
  </script>
</body>

</html>