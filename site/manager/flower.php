<!DOCTYPE html>
<html lang="ru">

<head>
  <title>Flowery - добавление цветка</title>
  <meta name="Author" content="author">
  <meta name="Description" content="description">
  <meta name="Keywords" content="keywords">
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="/styles.css">
  <script src="/js/jquery.min.js"></script>
  <script src="/js/index.js"></script>
</head>
<style>
</style>

<body>
  <!--
  <?php
	include "../menu.php";
	include "../database/database-open.php";
  ?>
-->
	<div class="main">
		<h2>Выберите цветок для букета</h2>
		<?php
			$query = 
				"SELECT id,title
				FROM flowers ";
			
			if ($result = pg_query($link,$query)) {
				while($row = pg_fetch_row($result)){
					echo "<div class=\"busket-list\" id=\"$row[0]\">";
					echo "	<div class=\"busket-item\" style=\"width: 100%; text-align: center;\">$row[1]</div>";
					echo "	<div class=\"busket-item\">";					
					echo "		<form method=\"POST\" action=\"logic/addFlower.php\">";
					echo "			<input type=submit value=\"Добавить\"/>";
					echo "			<input type=\"hidden\" name=\"added_item_id\" value=\"$row[0]\">";
					echo "		</form>";
					echo "	</div>";					
					echo "</div>";				
				}
			}
		?>
  </div>
  <div style="clear: both;"></div>
  <!--
    <?php
		include "../footer.php";
		include "../database/database-close.php";
	?>
-->
</body>

</html>