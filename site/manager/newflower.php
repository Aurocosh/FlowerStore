<!DOCTYPE html>
<html lang="ru">

<head>
  <title>Flowery - добавление цветка</title>
  <meta name="Author" content="author">
  <meta name="Description" content="description">
  <meta name="Keywords" content="keywords">
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="/styles.css">
  <script src="/js/jquery.min.js"></script>
  <script src="/js/index.js"></script>
</head>
<style>
.button_a{
  float: none;
  font-size: 18px;
  width: 200px;
  padding: 0;
}
</style>

<body>
  <?php
	include "../menu.php";
	include "../database/database-open.php";
  ?>
  <div class="main" align="center">
    <h2 style="margin-bottom: 10px;">Добавить новый цветок</h2>
      <div id="flower" >
        <form method = "post" action="logic/addNewFlower.php">
          <input name = "name" type="text" style="width: 198px" value="Название цветка" /> 
          <input class="button_a" type="submit" value="Добавить" />
        </form>
      </div>
  </div>
  <div style="clear: both;"></div>
    <?php
		include "../footer.php";
		include "../database/database-close.php";
	?>
</body>

</html>